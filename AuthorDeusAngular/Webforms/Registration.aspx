﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="AuthorDeusAngular.Webforms.Registration" %>

<!DOCTYPE html>

<script src="../Scripts/jquery-1.10.2.min.js"></script>
<script src="../Scripts/bootstrap.js"></script>
<script src="../Scripts/angular/angular.min.js"></script>
<script src="../Scripts/angular/angular-route.min.js"></script>
<script src="../Scripts/angular/angular-notify.min.js"></script>
<script src="../Scripts/angular/ngStorage.min.js"></script>
<script src="../Scripts/angular/ng-table.min.js"></script>
<script src="../Scripts/angular/angular-confirm.min.js"></script>
<script src="../Scripts/angular/angular-summernote.min.js"></script>
<script src="../Scripts/angular/angular-filter.min.js"></script>

<script src="../app/app.js"></script>

<script src="../app/services/userService.js"></script>
<script src="../app/services/userTypeServices.js"></script>

<script src="../app/controllers/user/userController.js"></script>
<script src="../app/controllers/user/userTypeController.js"></script>

<link href="../Content/bootstrap.min.css" rel="stylesheet" />
<link href="../Content/Site.css" rel="stylesheet" />
<link href="../Content/font-awesome.min.css" rel="stylesheet" />
<link href="../Content/AdminLTE/css/AdminLTE.min.css" rel="stylesheet" />
<link href="../Content/AngularNotify/angular-notify-material.min.css" rel="stylesheet" />

<%--<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">--%>

<html xmlns="http://www.w3.org/1999/xhtml" style="background-color: #F5F5F5" ng-app="appModule" ng-controller="UserController as userCtrl" ng-cloak>
<head runat="server">
    <title>Registration</title>
    <style>
        .content {
            min-height: 0px !important;
        }

        .bodyheight {
            max-height: 800px;
            background-color: #F5F5F5;
        }
    </style>
</head>
<body class="bodyheight">
    <div id="myOverlay" ng-if="userCtrl.loader"></div>
    <div id="loadingGIF">
        <img src="../Sources/redirect.gif" ng-if="userCtrl.loader" />
    </div>
    <div class="container">
        <%--     <div class="box box-primary" style="margin-top: 5%;">
            <div class="box-header with-border">
                <h3 class="box-title">Registration</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <form name="registerform" novalidate>
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-6 form-group" ng-class="{'has-error': registerform.inputfirstname.$invalid == true && registerform.inputfirstname.$pristine != true}">
                                    <label>First Name</label>
                                    <input ng-model="userCtrl.newUser.FirstName" ng-required="true" name="inputfirstname" type="text" placeholder="Enter First Name Here.." class="form-control" maxlength="20">
                                    <span class="help-block" ng-show="registerform.inputfirstname.$invalid && !registerform.inputfirstname.$pristine">First name is required</span>
                                </div>
                                <div class="col-sm-6 form-group" ng-class="{'has-error': registerform.inputlastname.$invalid == true && registerform.inputlastname.$pristine != true }">
                                    <label>Last Name</label>
                                    <input ng-model="userCtrl.newUser.LastName" ng-required="true" name="inputlastname" type="text" placeholder="Enter Last Name Here.." class="form-control" maxlength="20">
                                    <span class="help-block" ng-show="registerform.inputlastname.$invalid && !registerform.inputlastname.$pristine">Last name is required</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group" ng-class="{'has-error': registerform.inputpassword.$invalid == true && registerform.inputpassword.$pristine != true}">
                                    <label>Password</label>
                                    <input ng-model="userCtrl.newUser.Password" ng-required="true" name="inputpassword" type="password" placeholder="Enter Password Here.." class="form-control" maxlength="20">
                                </div>
                                <div class="col-sm-6 form-group" ng-class="{'has-error': userCtrl.newUser.Password != userCtrl.confirmPassword && registerform.inputpassword.$pristine == false}">
                                    <label>Confirm Password</label>
                                    <input ng-model="userCtrl.confirmPassword" type="password" placeholder="Re-Enter Password Here.." class="form-control" maxlength="20">
                                    <span class="help-block" ng-show="userCtrl.newUser.Password != userCtrl.confirmPassword && !registerform.inputpassword.$pristine">Must be the same with password</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group" ng-class="{'has-error': registerform.inputusername.$invalid == true && registerform.inputusername.$pristine != true}" ng-init="userCtrl.usernameWarning = 'Username required'">
                                    <label>Username</label>
                                    <input ng-model="userCtrl.newUser.Username"   ng-blur="userCtrl.checkUsername()" ng-required="true" name="inputusername" type="text" placeholder="Enter Username Here.." class="form-control" maxlength="15">
                                    <span class="help-block" ng-show="registerform.inputusername.$invalid && !registerform.inputusername.$pristine">{{userCtrl.usernameWarning}}</span>
                                </div>
                                <div class="col-sm-6 form-group" ng-class="{'has-error': registerform.inputemail.$invalid == true && registerform.inputemail.$pristine != true}" ng-init="userCtrl.emailWarning = 'Email is invalid/required'">
                                    <label>Email Address</label>
                                    <input ng-model="userCtrl.newUser.Email" ng-blur="userCtrl.checkEmail()" ng-required="true" name="inputemail" type="email" placeholder="Enter Email Address Here.." class="form-control"
                                        ng-pattern="/^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/">
                                    <span class="help-block" ng-show="registerform.inputemail.$invalid && !registerform.inputemail.$pristine">{{userCtrl.emailWarning}}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 form-group" ng-class="{'has-error': registerform.inputcity.$invalid == true && registerform.inputcity.$pristine != true}">
                                    <label>City</label>
                                    <input ng-model="userCtrl.newUser.City" ng-required="true" name="inputcity" type="text" placeholder="Enter City Name Here.." class="form-control" maxlength="20">
                                    <span class="help-block" ng-show="registerform.inputcity.$invalid && !registerform.inputcity.$pristine">City is required</span>
                                </div>
                                <div class="col-sm-4 form-group">
                                    <label>State</label>
                                    <input ng-model="userCtrl.newUser.State" type="text" placeholder="Enter State Name Here.." class="form-control" maxlength="20">
                                </div>
                                <div class="col-sm-4 form-group">
                                    <label>Zip</label>
                                    <input ng-model="userCtrl.newUser.Zip" type="text" placeholder="Enter Zip Code Here.." class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>Phone Number</label>
                                    <input ng-model="userCtrl.newUser.ContactNo" type="text" placeholder="Enter Phone Number Here.." class="form-control">
                                </div>
                                <div class="col-sm-6 form-group" ng-controller="UserTypeController as userTypeCtrl">
                                    <label>User Type</label>
                                    <select
                                        ng-model="userCtrl.selectedUserType"
                                        ng-options="usertype as usertype.Name for usertype in userTypeCtrl.userTypes"
                                        class="form-control">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <textarea ng-model="userCtrl.newUser.Address" placeholder="Enter Address Here.." rows="3" class="form-control" maxlength="100"></textarea>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-lg btn-primary" ng-click="userCtrl.createUser()"
                                        ng-disabled="!registerform.$valid">
                                        Register</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>--%>

        <div class="register-box">
            <div class="register-logo">
                <a href="../../index2.html"><b>AuthorDeus</b></a>
            </div>

            <div class="register-box-body">
                <p class="login-box-msg">Register a new membership</p>

                <form name="registerform" novalidate>
                    <div class="form-group has-feedback" ng-class="{'has-error': registerform.inputfirstname.$invalid == true && registerform.inputfirstname.$pristine != true}">
                        <input ng-model="userCtrl.newUser.FirstName" ng-required="true" name="inputfirstname" type="text" placeholder="Enter First Name Here.." class="form-control" maxlength="20">
                        <span class="help-block" ng-show="registerform.inputfirstname.$invalid && !registerform.inputfirstname.$pristine">First name is required</span>
                        <span class="glyphicon glyphicon-info-sign form-control-feedback"></span>
                    </div>
                    <div class="form-group  has-feedback" ng-class="{'has-error': registerform.inputlastname.$invalid == true && registerform.inputlastname.$pristine != true }">
                        <input ng-model="userCtrl.newUser.LastName" ng-required="true" name="inputlastname" type="text" placeholder="Enter Last Name Here.." class="form-control" maxlength="20">
                        <span class="help-block" ng-show="registerform.inputlastname.$invalid && !registerform.inputlastname.$pristine">Last name is required</span>
                        <span class="glyphicon  glyphicon-info-sign form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback" ng-class="{'has-error': registerform.inputusername.$invalid == true && registerform.inputusername.$pristine != true}" ng-init="userCtrl.usernameWarning = 'Username required'">
                        <input ng-model="userCtrl.newUser.Username" ng-blur="userCtrl.checkUsername()" ng-required="true" name="inputusername" type="text" placeholder="Enter Username Here.." class="form-control" maxlength="15">
                        <span class="glyphicon  glyphicon-info-sign form-control-feedback"></span>
                        <span class="help-block" ng-show="registerform.inputusername.$invalid && !registerform.inputusername.$pristine">{{userCtrl.usernameWarning}}</span>
                    </div>
                    <div class="form-group has-feedback" ng-class="{'has-error': registerform.inputemail.$invalid == true && registerform.inputemail.$pristine != true}" ng-init="userCtrl.emailWarning = 'Email is invalid/required'">
                        <input ng-model="userCtrl.newUser.Email" ng-blur="userCtrl.checkEmail()" ng-required="true" name="inputemail" type="email" placeholder="Enter Email Address Here.." class="form-control"
                            ng-pattern="/^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/">
                        <span class="help-block" ng-show="registerform.inputemail.$invalid && !registerform.inputemail.$pristine">{{userCtrl.emailWarning}}</span>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback" ng-class="{'has-error': registerform.inputpassword.$invalid == true && registerform.inputpassword.$pristine != true}">
                        <input ng-model="userCtrl.newUser.Password" ng-required="true" name="inputpassword" type="password" placeholder="Enter Password Here.." class="form-control" maxlength="20">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback" ng-class="{'has-error': userCtrl.newUser.Password != userCtrl.confirmPassword && registerform.inputpassword.$pristine == false}">
                        <input ng-model="userCtrl.confirmPassword" type="password" placeholder="Retype password" class="form-control" maxlength="20">
                        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                        <span class="help-block" ng-show="userCtrl.newUser.Password != userCtrl.confirmPassword && !registerform.inputpassword.$pristine">Must be the same with password</span>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" ng-required="true" ng-model="userCtrl.agreeCheckbox">
                                    I agree to the <a href="#">terms</a>
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat" ng-click="userCtrl.createUser()"
                                ng-disabled="!registerform.$valid">
                                Register</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>

                <%--                <div class="social-auth-links text-center">
                    <p>- OR -</p>
                    <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i>Sign up using
        Facebook</a>
                    <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i>Sign up using
        Google+</a>
                </div>

                <a href="login.html" class="text-center">I already have a membership</a>--%>
            </div>
            <!-- /.form-box -->
        </div>

        <div notifybar style="position: absolute; top: 0; right: 0"></div>
    </div>
</body>

</html>
