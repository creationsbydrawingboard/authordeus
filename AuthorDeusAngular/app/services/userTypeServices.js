﻿angular.module('appModule').factory("UserTypeService", function ($http) {

    var globalConfig = 'contenttype';

    return {

        //since $http.get returns a promise,
        //and promise.then() also returns a promise
        //that resolves to whatever value is returned in it's 
        //callback argument, we can return that.

        getUserTypes: function (url) {
            return $http.get(url).then(function (result) {
                return result.data;
            });
        }

    }

});