﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthorDeusData.Models
{
  public class UserDTO
  {

    public int UserId { get; set; }
    public int? InviteId { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public Nullable<int> UserTypeId { get; set; }
    public string UserTypeDescription { get; set; }
    public string Address { get; set; }
    public string City { get; set; }
    public string State { get; set; }
    public string Zip { get; set; }
    public string ContactNo { get; set; }
    public string Email { get; set; }
    public Nullable<bool> Void { get; set; }

  }
}