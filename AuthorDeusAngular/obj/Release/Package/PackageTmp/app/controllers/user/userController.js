﻿'USE STRICT';

angular.module('appModule').controller('UserController', ['$scope', '$http', 'UserService', 'BASE_USER_ENDPOINT', '$window', '$timeout', '$sessionStorage', 'NgTableParams', '$ngConfirm',
    function ($scope, $http, UserService, BASE_USER_ENDPOINT, $window, $timeout, $sessionStorage, NgTableParams, $ngConfirm) {

        /*<---------------- Declarations --------------> */

        var vm = this;
        var _userService = UserService;
        var url = BASE_USER_ENDPOINT + 'Users';

        vm.getUsers = getUsers;
        vm.getUserList = getUserList;
        vm.getUserById = getUserById;
        vm.createUser = createUser;
        vm.updateUser = updateUser;
        vm.deleteUser = deleteUser;
        vm.checkEmail = checkEmail;
        vm.checkUsername = checkUsername;

        vm.userList = [];
        vm.errorLog;

        vm.user = {
            UserId: "", Username: "", Password: "", FirstName: "", LastName: "", UserTypeId: "", Address: "", City: "", State: "",
            Zip: "", ContactNo: "", Email: "", DateCreated: "", CreatedBy: "", DateUpdated: "", UpdatedBy: "", Void: false
        };

        vm.newUser = {
            Username: "", Password: "", FirstName: "", LastName: "", UserTypeId: "", Address: "", City: "", State: "",
            Zip: "", ContactNo: "", Email: "", DateCreated: "", CreatedBy: "", DateUpdated: "", UpdatedBy: "", Void: false
        };

        vm.loader = false;
        vm.try = true;
        vm.selectedUserType;
        vm.currentUser;
        vm.emailWarning;

        vm.pristineUser = angular.copy(vm.user);

        /*<---------------- Declarations --------------> */

        /*<---------------- Functions --------------> */

        //Created by: Patrick Sacay
        //Definition: initialize controller if existing data in session
        function initializePage() {
            if ($sessionStorage.SessionData != undefined || $sessionStorage.SessionData != null) {
                vm.currentUser = $sessionStorage.SessionData;
                populateUserProfile();
            }
        }

        //Created by: Patrick Sacay
        //Definition: Get all users in the database
        function getUsers() {
            _userService.getUsers().then(function (data) {
                //this will execute when the AJAX call completes.
                vm.userList = data;
            });

        };

        //Created by: Patrick Sacay
        //Definition: Get user by Id in database
        function getUserById() {

        };

        //Created by: Patrick Sacay
        //Definition: create user record in database
        function createUser() {

            var dateTime = new Date();

            vm.newUser.UserTypeId = vm.selectedUserType.UserTypeId;
            vm.newUser.DateCreated = dateTime;
            vm.newUser.DateUpdated = dateTime;
            vm.newUser.CreatedBy = 'system'; //change this to session name
            vm.newUser.UpdatedBy = 'system';

            _userService.createUser(url, vm.newUser).then(function (data) {
                if (data) {

                    showNotification('success', 'Successfully Created User');
                    //this is used to clear the form
                    $scope.registerform.$setPristine();
                    vm.user = vm.pristineUser;

                    //redirecting to homepage loading
                    vm.loader = true;

                    //redirect to page
                    $sessionStorage.SessionData = data.data;

                    $timeout(function () {
                        $window.location.href = '../../../Default.aspx';
                    }, 2000);
                }

            })
                .catch(function (err) {

                });

        };

        //Created by: Patrick Sacay
        //Definition: show notification of successful user creation
        function showNotification(ntype, ntext) {
            var notify = {
                type: ntype,
                title: ntext,
                timeout: 1000 //time in ms
            };

            $scope.$emit('notify', notify);
        }

        //Created by: Patrick Sacay
        //Definition: create user record in database
        function populateUserProfile() {

            vm.user.UserId = vm.currentUser.UserId
            vm.user.Username = vm.currentUser.Username;
            vm.user.Password = vm.currentUser.Password;
            vm.confirmPassword = vm.currentUser.Password;
            vm.user.FirstName = vm.currentUser.FirstName;
            vm.user.LastName = vm.currentUser.LastName;
            vm.user.UserTypeId = vm.currentUser.UserTypeId;
            vm.user.Address = vm.currentUser.Address;
            vm.user.City = vm.currentUser.City;
            vm.user.State = vm.currentUser.State;
            vm.user.Zip = vm.currentUser.Zip;
            vm.user.ContactNo = vm.currentUser.ContactNo;
            vm.user.Email = vm.currentUser.Email;
            vm.user.DateCreated = vm.currentUser.DateCreated;
            vm.user.CreatedBy = vm.currentUser.CreatedBy;
            vm.user.DateUpdated = vm.currentUser.DateUpdated;
            vm.user.UpdatedBy = vm.currentUser.UpdatedBy;
            vm.user.Void = vm.currentUser.Void;

        }

        //Created by: Patrick Sacay
        //Definition: update user record in database
        function updateUser() {

            if (!vm.enableform) {
                vm.enableform = true;
                vm.userProfileText = 'Save Changes';
                vm.UsernameDisabled = true;
                vm.EmailDisabled = true;
            }
            else {
                updateUserRecord();
                vm.enableform = false;
                vm.userProfileText = 'Update';
           
            }
        }

        //Created by: Patrick Sacay
        //Definition: create user record in database
        function updateUserRecord() {

            var dateTime = new Date();

            vm.user.UserTypeId = vm.selectedUserType.UserTypeId;
            vm.user.DateUpdated = dateTime;
            vm.user.UpdatedBy = vm.currentUser.Username;

            _userService.updateUser(url, vm.user).then(function (data) {
                if (data) {
                    showNotification('success', 'Sucessfully Updated User');
                    $sessionStorage.SessionData = vm.user;
                }

            })
                .catch(function (err) {
                    showNotification('error', 'Something went wrong in updating user.');
                });

        };

        function getUserList() {

            _userService.getUserList(url).then(function (response) {
                if (response) {
                    vm.userList = response.data;
                }

            })
                .catch(function (err) {
                    showNotification('error', 'Something went wrong in updating user.');
                });

        }

        function deleteUser(user) {


            ////  showNotification('success', 'Successfully Created User');

            $ngConfirm({
                title: 'Deleting user',
                content: 'Are you sure you want to delete user?',
                autoClose: 'cancel|8000',
                buttons: {
                    deleteUser: {
                        text: 'delete user',
                        btnClass: 'btn-red',
                        action: function () {
                            deleteUserRecord(user);
                        }
                    },
                    cancel: function () {
                        $ngConfirm('action is canceled');
                    }
                }
            });

        }

        function deleteUserRecord(user) {

            var dateTime = new Date();

            user.DateUpdated = dateTime;
            user.UpdatedBy = vm.currentUser.Username;
            user.Void = true;

            _userService.updateUser(url, user).then(function (data) {
                if (data) {
                    showNotification('information', 'Sucessfully Removed User');
                    getUserList();
                }

            })
                .catch(function (err) {
                    showNotification('error', 'Something went wrong in updating user.');
                });

        }

        function checkEmail() {

            if (vm.newUser.Email != undefined) {


                _userService.verifyEmail(url, vm.newUser.Email).then(function (response) {
                    if (response.data != null) {
                        $scope.registerform.inputemail.$invalid = true;
                        $scope.registerform.inputemail.$pristine = false;
                        vm.emailWarning = 'Email is already taken';
                    }

                })
                    .catch(function (err) {
                        showNotification('error', 'Something went wrong.');
                    });
            }

        }


        function checkUsername() {

            if (vm.newUser.Username != undefined) {

                _userService.verifyUsername(url, vm.newUser.Username).then(function (response) {
                    if (response.data != null) {
                        $scope.registerform.inputusername.$invalid = true;
                        $scope.registerform.inputusername.$pristine = false;
                        vm.usernameWarning = 'Username is already taken';
                    }

                })
                    .catch(function (err) {
                        showNotification('error', 'Something went wrong.');
                    });
            }

        }

        /*<---------------- Functions --------------> */


        /*<---------------- Function Calls --------------> */

        ////getUsers();

        initializePage();

        /*<---------------- Function Calls --------------> */


    }]);