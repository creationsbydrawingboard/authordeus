﻿
angular.module('appModule').factory("UserService", ['$http', function ($http) {

    var globalConfig = 'contenttype';

    return {

        //since $http.get returns a promise,
        //and promise.then() also returns a promise
        //that resolves to whatever value is returned in it's 
        //callback argument, we can return that.

        //** Dont forget to put forward slash before methods path(see ex[1])

        getUsers: function (url) {

            return $http.get(url).then(function (result) {
                return result.data;
            });
        },

        verifyUser: function (url, user) {
            var param = JSON.stringify(user);

            //ex[1]
            var method = "/VerifyUser";

            url = url + method, data = param;

            return $http.post(url, data, globalConfig).then(function (result) {
                return result;
            });
        },

        verifyEmail: function (url, email) {          

            var method = "/CheckEmail?email=" + email;

            url = url + method;

            return $http.post(url, globalConfig).then(function (result) {
                return result;
            });
        },

        verifyUsername: function (url, username) {

            var method = "/CheckUsername?username=" + username;

            url = url + method;

            return $http.post(url, globalConfig).then(function (result) {
                return result;
            });
        },

        createUser: function (url, user) {
            var param = JSON.stringify(user);
            var data = param;

            return $http.post(url, data, globalConfig).then(function (result) {
                return result;
            });
        },

        getAssignedUsers: function (url, userid, projectid) {
            
            var method = "/GetAssignedUsers?userId=" + userid + "&projectId=" + projectid;

            url = url + method;

            return $http.get(url).then(function (response) {
                return response.data;
            })
        },

        updateUser: function (url, user)
        {
            var param = JSON.stringify(user);
            var data = param;

            return $http.put(url, data, globalConfig).then(function (result) {
                return result;
            });
        },

        getUserList: function (url) {

            var method = "/UserList";

            url = url + method;

            return $http.get(url, globalConfig).then(function (result) {
                return result;
            });
        }

    }


}]);