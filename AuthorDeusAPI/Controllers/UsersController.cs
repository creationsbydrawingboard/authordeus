﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AuthorDeusData;
using AuthorDeusData.Models;

namespace AuthorDeusAPI.Controllers
{
  public class UsersController : ApiController
  {
    private AuthorDeusEntities db = new AuthorDeusEntities();
    

    public UsersController()
    {
      db.Configuration.ProxyCreationEnabled = false;
    }

    // GET: api/Users
    public IQueryable<User> GetUser()
    {
      return db.User;
    }

    /// <summary>
    /// Gets user data based on id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    // GET: api/Users/5
    [ResponseType(typeof(User))]
    public async Task<IHttpActionResult> GetUser(int id)
    {
      User user = await db.User.FindAsync(id);
      if (user == null)
      {
        return NotFound();
      }

      return Ok(user);
    }

    /// <summary>
    /// Check if user is in the database
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>
    // POST: api/Users/5
    [ResponseType(typeof(UserDTO))]
    [Route("api/Users/VerifyUser")]
    public async Task<IHttpActionResult> VerifyUser([FromBody]UserDTO user)
    {

      UserDTO validuser = await (from p in db.User
                              where
                              p.Username == user.Username
                              && p.Password == user.Password && p.Void == false
                              select new UserDTO
                              {
                                UserId = p.UserId,
                                Username = p.Username,
                                Password = p.Password,
                                FirstName = p.FirstName,
                                LastName = p.LastName,
                                UserTypeId = p.UserTypeId,
                                UserTypeDescription = p.UserType.Description,
                                Address = p.Address,
                                City = p.City,
                                State = p.State,
                                Zip = p.Zip,
                                ContactNo = p.ContactNo,
                                Email = p.Email
                              }).FirstAsync();
      if (validuser == null)
      {
        return NotFound();
      }

      return Ok(validuser);
    }

    /// <summary>
    /// check if email is existing in database
    /// </summary>
    /// <param name="email"></param>
    /// <returns></returns>
    // POST: api/Users/5
    [ResponseType(typeof(User))]
    [Route("api/Users/CheckEmail")]
    public async Task<IHttpActionResult> CheckEmail(string email)
    {
      try
      {
        User existingemail = await (from p in db.User
                                    where p.Email == email && p.Void == false
                                    select p).FirstOrDefaultAsync();
        if (existingemail == null)
        {
          return Ok(existingemail);
        }

        return Ok(existingemail);
      }
      catch (Exception ex)
      {
        return NotFound();
      }

    }

    /// <summary>
    /// check if username is existing in database
    /// </summary>
    /// <param name="username"></param>
    /// <returns></returns>
    [ResponseType(typeof(User))]
    [Route("api/Users/CheckUsername")]
    public async Task<IHttpActionResult> CheckUsername(string username)
    {
      try
      {
        User existingusername = await (from p in db.User
                                       where p.Username == username && p.Void == false
                                       select p).FirstOrDefaultAsync();
        if (existingusername == null)
        {
          return Ok(existingusername);
        }

        return Ok(existingusername);
      }
      catch (Exception ex)
      {
        return NotFound();
      }

    }

    /// <summary>
    /// update user record in database
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>
    // PUT: api/Users/5
    [ResponseType(typeof(void))]
    public async Task<IHttpActionResult> PutUser(User user)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      //if (id != user.UserId)
      //{
      //	return BadRequest();
      //}

      db.Entry(user).State = EntityState.Modified;

      try
      {
        await db.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!UserExists(user.UserId))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return StatusCode(HttpStatusCode.NoContent);
    }

    /// <summary>
    /// Create user record in the database
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>
    // POST: api/Users
    [ResponseType(typeof(User))]
    public async Task<IHttpActionResult> PostUser(User user)
    {
      try
      {
        if (!ModelState.IsValid)
        {
          return BadRequest(ModelState);
        }

        db.User.Add(user);
        await db.SaveChangesAsync();

      }
      catch (Exception)
      {
        return BadRequest();
        throw;
      }

      return Ok(user);
    }

    /// <summary>
    /// Hard delete user in the record
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    // DELETE: api/Users/5
    [ResponseType(typeof(User))]
    public async Task<IHttpActionResult> DeleteUser(int id)
    {
      User user = await db.User.FindAsync(id);
      if (user == null)
      {
        return NotFound();
      }

      db.User.Remove(user);
      await db.SaveChangesAsync();

      return Ok(user);
    }

    /// <summary>
    /// Load user list in the database
    /// </summary>
    /// <returns></returns>
    // GET: api/Users/5
    [ResponseType(typeof(List<User>))]
    [Route("api/Users/UserList")]
    public async Task<IHttpActionResult> GetUserList()
    {
      //// db.Configuration.ProxyCreationEnabled = false;

      List<User> userList = await (from p in db.User
                                   where p.UserTypeId != 1000 && p.Void != true
                                   select p).ToListAsync();
      return Ok(userList);
    }


    /// <summary>
    /// load assigned user based on project id
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="projectId"></param>
    /// <returns></returns>
    // GET: api/Users/5
    [ResponseType(typeof(List<User>))]
    [Route("api/Users/GetAssignedUsers")]
    public async Task<IHttpActionResult> GetAssignedUsers(int userId, int projectId)
    {

      List<User> userList = new List<User>();

      userList = await (from p in db.User
                        join q in db.ProjectGroup on
                        p.UserId equals q.UserId
                        where q.ProjectId == projectId && p.UserId != userId && q.Void == false
                        select p).ToListAsync();

      if (userList == null)
      {
        return NotFound();
      }

      return Ok(userList);
    }

    /// <summary>
    /// load printer list
    /// </summary> 
    /// <param name="projectId"></param>
    /// <returns></returns>
    // GET: api/Users/5
    [ResponseType(typeof(List<UserDTO>))]
    [Route("api/Users/GetPrinterInvitation")]
    public async Task<IHttpActionResult> GetPrinterInvitation(int projectId)
    {

      List<UserDTO> userList = new List<UserDTO>();

      userList = await (from p in db.User
                        from pq in db.QuoteInvite.Where(x => x.PrinterId == p.UserId && x.ProjectId == projectId).DefaultIfEmpty()
                        where p.UserTypeId == 1001 &&
                        p.Void == false
                        select new UserDTO
                        {
                          UserId = p.UserId,
                          InviteId = pq.InviteId,
                          FirstName = p.FirstName,
                          LastName = p.LastName,
                          Email = p.Email,
                          ContactNo = p.ContactNo
                        }).ToListAsync();

      if (userList == null)
      {
        return NotFound();
      }

      return Ok(userList);
    }


    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        db.Dispose();
      }
      base.Dispose(disposing);
    }

    private bool UserExists(int id)
    {
      return db.User.Count(e => e.UserId == id) > 0;
    }
  }
}