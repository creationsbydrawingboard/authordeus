﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AuthorDeusAngular._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div ng-controller="HomeController as home">
        <div notifybar style="position: absolute; top: 0; right: 0; z-index: 999999"></div>
        <div>
            <h1>Welcome {{home.currentUser.FirstName}}</h1>
            <p class="lead">AuthorDeus is a web platform for writers and publishers alike.</p>
        </div>
        <div>
            <h3>Timeline       
            </h3>
        </div>
        <div class="row" ng-controller="NotificationController as notifyCtrl">
            <div class="col-lg-12 col-md-12">

                <ul class="timeline timeline-inverse" ng-repeat="(key, value) in notifyCtrl.notificationList | groupBy: 'DateCreated'">

                    <!-- timeline time label -->
                    <li class="time-label">
                        <span class="bg-red">{{ key | date:'longDate' }}
                        </span>
                    </li>

                    <!-- timeline item -->

                    <li ng-repeat="notification in value">
                        <i class="fa fa-envelope bg-blue"></i>
                        <div class="timeline-item">
                            <h3 class="timeline-header no-border">{{ notification.Description }} 
                            </h3>
                        </div>
                    </li>

                  <%--  <li>
                        <i class="fa fa-envelope bg-blue"></i>
                        <div class="timeline-item">
                            <h3 class="timeline-header no-border">
                                <a href="#">Sarah Young</a> accepted your friend request
                            </h3>
                        </div>
                    </li>

                    <li ng-repeat="notification in value">player: {{ notification.Description }} 
                    </li>--%>
                </ul>


           <%--     <ul class="timeline timeline-inverse">
                    <!-- timeline time label -->
                    <li class="time-label">
                        <span class="bg-red">10 Feb. 2014
                        </span>
                    </li>
                    <!-- /.timeline-label -->
                    <!-- timeline item -->
                    <li>
                        <i class="fa fa-envelope bg-blue"></i>

                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i>12:05</span>

                            <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>

                            <div class="timeline-body">
                                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                        weebly ning heekya handango imeem plugg dopplr jibjab, movity
                        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                        quora plaxo ideeli hulu weebly balihoo...
                            </div>
                            <div class="timeline-footer">
                                <a class="btn btn-primary btn-xs">Read more</a>
                                <a class="btn btn-danger btn-xs">Delete</a>
                            </div>
                        </div>
                    </li>
                    <!-- END timeline item -->
                    <!-- timeline item -->
                    <li>
                        <i class="fa fa-user bg-aqua"></i>

                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i>5 mins ago</span>

                            <h3 class="timeline-header no-border"><a href="#">Sarah Young</a> accepted your friend request
                            </h3>
                        </div>
                    </li>
                    <!-- END timeline item -->
                    <!-- timeline item -->
                    <li>
                        <i class="fa fa-comments bg-yellow"></i>

                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i>27 mins ago</span>

                            <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>

                            <div class="timeline-body">
                                Take me to your leader!
                        Switzerland is small and neutral!
                        We are more like Germany, ambitious and misunderstood!
                            </div>
                            <div class="timeline-footer">
                                <a class="btn btn-warning btn-flat btn-xs">View comment</a>
                            </div>
                        </div>
                    </li>
                    <!-- END timeline item -->
                    <!-- timeline time label -->
                    <li class="time-label">
                        <span class="bg-green">3 Jan. 2014
                        </span>
                    </li>
                    <!-- /.timeline-label -->
                    <!-- timeline item -->
                    <li>
                        <i class="fa fa-camera bg-purple"></i>

                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i>2 days ago</span>

                            <h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>

                            <div class="timeline-body">
                                <img src="http://placehold.it/150x100" alt="..." class="margin">
                                <img src="http://placehold.it/150x100" alt="..." class="margin">
                                <img src="http://placehold.it/150x100" alt="..." class="margin">
                                <img src="http://placehold.it/150x100" alt="..." class="margin">
                            </div>
                        </div>
                    </li>
                    <!-- END timeline item -->
                    <li>
                        <i class="fa fa-clock-o bg-gray"></i>
                    </li>
                </ul>--%>
            </div>
        </div>

    </div>
</asp:Content>
