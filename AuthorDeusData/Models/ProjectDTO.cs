﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthorDeusData.Models
{
  public class ProjectDTO
  {
    public int ProjectId { get; set; }
    public Nullable<int> OwnerId { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public string Data { get; set; }
    public Nullable<System.DateTime> DateCreated { get; set; }
    public string CreatedBy { get; set; }
    public Nullable<System.DateTime> DateUpdated { get; set; }
    public string UpdatedBy { get; set; }
    public Nullable<bool> Void { get; set; }

  }
}