﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserProfile.aspx.cs" Inherits="AuthorDeusAngular.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script src="../../app/controllers/user/userController.js"></script>
    <script src="../../app/controllers/user/userTypeController.js"></script>
    <script src="../../app/services/userService.js"></script>
    <script src="../../app/services/userTypeServices.js"></script>

    <div class="row" ng-controller="UserController as userCtrl">
        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="../../Sources/Images/avatar5.png" alt="User profile picture">
                    <h3 class="profile-username text-center">{{userCtrl.currentUser.FirstName}} {{userCtrl.currentUser.LastName}}</h3>

                    <p class="text-muted text-center">Software Engineer</p>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Projects</b> <a class="pull-right">1,322</a>
                        </li>
                        <li class="list-group-item">
                            <b>Followers</b> <a class="pull-right">543</a>
                        </li>
                        <li class="list-group-item">
                            <b>Friends</b> <a class="pull-right">13,287</a>
                        </li>
                    </ul>

                    <b>
                        <input type="button" class="btn btn-primary btn-block" ng-click="userCtrl.updateUser()" ng-init="userCtrl.enableform = false; userCtrl.userProfileText = 'Update'" value="{{userCtrl.userProfileText}}"></b>
                </div>
                <!-- /.box-body -->
            </div>

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">About Me</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <strong><i class="fa fa-book margin-r-5"></i>Education</strong>
                    <p class="text-muted">
                        B.S. in Computer Science from the University of Tennessee at Knoxville
                    </p>
                    <hr>
                    <strong><i class="fa fa-map-marker margin-r-5"></i>Location</strong>
                    <p class="text-muted">{{userCtrl.currentUser.Address}}</p>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
        <div class="col-md-9 col-lg-9 col-sm-6 col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Profile Details</h3>
                </div>
                <div class="box-body">
                    <fieldset ng-disabled="!userCtrl.enableform">
                        <div class="form" name="registerform">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6 form-group" ng-class="{'has-error': registerform.inputfirstname.$invalid == true && registerform.inputfirstname.$pristine != true}">
                                        <label>First Name</label>
                                        <input ng-model="userCtrl.user.FirstName" ng-required="true" name="inputfirstname" type="text" placeholder="Enter First Name Here.." class="form-control" maxlength="20">
                                        <span class="help-block" ng-show="registerform.inputfirstname.$invalid && !registerform.inputfirstname.$pristine">First name is required</span>
                                    </div>
                                    <div class="col-sm-6 form-group" ng-class="{'has-error': registerform.inputlastname.$invalid == true && registerform.inputlastname.$pristine != true }">
                                        <label>Last Name</label>
                                        <input ng-model="userCtrl.user.LastName" ng-required="true" name="inputlastname" type="text" placeholder="Enter Last Name Here.." class="form-control" maxlength="20">
                                        <span class="help-block" ng-show="registerform.inputlastname.$invalid && !registerform.inputlastname.$pristine">Last name is required</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 form-group" ng-class="{'has-error': registerform.inputpassword.$invalid == true && registerform.inputpassword.$pristine != true}">
                                        <label>Password</label>
                                        <input ng-model="userCtrl.user.Password" ng-required="true" name="inputpassword" type="password" placeholder="Enter Password Here.." class="form-control" maxlength="20">
                                    </div>
                                    <div class="col-sm-6 form-group" ng-class="{'has-error': userCtrl.user.Password != userCtrl.confirmPassword && registerform.inputpassword.$pristine == false}">
                                        <label>Confirm Password</label>
                                        <input ng-model="userCtrl.confirmPassword" type="password" placeholder="Re-Enter Password Here.." class="form-control" maxlength="20">
                                        <span class="help-block" ng-show="userCtrl.user.Password != userCtrl.confirmPassword && !registerform.inputpassword.$pristine">Must be the same with password</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 form-group" ng-class="{'has-error': registerform.inputusername.$invalid == true && registerform.inputusername.$pristine != true}">
                                        <label>Username</label>
                                        <input ng-model="userCtrl.user.Username" ng-required="true" ng-disabled="userCtrl.UsernameDisabled" name="inputusername" type="text" placeholder="Enter Username Here.." class="form-control" maxlength="15">
                                        <span class="help-block" ng-show="registerform.inputusername.$invalid && !registerform.inputusername.$pristine">Username is required</span>
                                    </div>
                                    <div class="col-sm-6 form-group" ng-class="{'has-error': registerform.inputemail.$invalid == true && registerform.inputemail.$pristine != true}">
                                        <label>Email Address</label>
                                        <input ng-model="userCtrl.user.Email" ng-required="true" ng-disabled="userCtrl.UsernameDisabled" name="inputemail" type="email" placeholder="Enter Email Address Here.." class="form-control"
                                            ng-pattern="/^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/">
                                        <span class="help-block" ng-show="registerform.inputemail.$invalid && !registerform.inputemail.$pristine">{{}}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 form-group" ng-class="{'has-error': registerform.inputcity.$invalid == true && registerform.inputcity.$pristine != true}">
                                        <label>City</label>
                                        <input ng-model="userCtrl.user.City" ng-required="true" name="inputcity" type="text" placeholder="Enter City Name Here.." class="form-control" maxlength="20">
                                        <span class="help-block" ng-show="registerform.inputcity.$invalid && !registerform.inputcity.$pristine">City is required</span>
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>State</label>
                                        <input ng-model="userCtrl.user.State" type="text" placeholder="Enter State Name Here.." class="form-control" maxlength="20">
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>Zip</label>
                                        <input ng-model="userCtrl.user.Zip" type="text" placeholder="Enter Zip Code Here.." class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <label>Phone Number</label>
                                        <input ng-model="userCtrl.user.ContactNo" type="text" placeholder="Enter Phone Number Here.." class="form-control">
                                    </div>
                                    <div class="col-sm-6 form-group" ng-controller="UserTypeController as userTypeCtrl">
                                        <label>User Type</label>
                                        <select
                                            ng-model="userCtrl.selectedUserType"
                                            ng-options="usertype as usertype.Name for usertype in userTypeCtrl.userTypes"
                                            class="form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea ng-model="userCtrl.user.Address" placeholder="Enter Address Here.." rows="3" class="form-control" maxlength="100"></textarea>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div notifybar style="position: absolute; top: 0; right: 0; z-index: 999999"></div>
    </div>
</asp:Content>
