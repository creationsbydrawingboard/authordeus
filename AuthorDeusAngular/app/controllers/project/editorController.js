﻿'USE STRICT';

angular.module('appModule').controller('EditorController', ['$scope', '$http', 'BASE_USER_ENDPOINT', '$window', '$timeout', '$sessionStorage', '$ngConfirm', 'ProjectService',
    function ($scope, $http, BASE_USER_ENDPOINT, $window, $timeout, $sessionStorage, $ngConfirm, ProjectService) {

        /*<---------------- Declarations --------------> */

        var vm = this;
        var url = BASE_USER_ENDPOINT + 'Projects';
        var userurl = BASE_USER_ENDPOINT + 'Users';
        var _projectService = ProjectService;

        vm.projectContent = "";
        vm.projectDataId = $sessionStorage.SessionDataProject;

        vm.updateProjectRecord = updateProjectRecord;

        vm.options = {
            height: 750,
            focus: true,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['misc', ['print']],
            ]
        };


        /*<---------------- Declarations --------------> */

        /*<---------------- Functions --------------> */

        //Created by: Patrick Sacay
        //Definition: initialize page using project controller
        function initializePage() {
            if ($sessionStorage.SessionData != undefined || $sessionStorage.SessionData != null) {
                vm.currentUser = $sessionStorage.SessionData;

                getProjectDetails(vm.projectDataId);

            }

        }

        //Created by: Patrick Sacay
        //Definition: get project details based on projectId provided
        function getProjectDetails(projectDataId) {
            _projectService.getProjectDetails(url, projectDataId).then(function (response) {
                //this will execute when the AJAX call completes.

                if (response != null) {
                    vm.projectData = response;
                    vm.projectContent = response.Data;
                }
                else {
                }

            });
        }

        //Created by: Patrick Sacay
        //Definition: update project record on database
        function updateProjectRecord() {

            var dateTime = new Date();

            vm.projectData.Data = vm.projectContent;

            vm.projectData.DateUpdated = dateTime;
            vm.projectData.UpdatedBy = vm.currentUser.Username;           

            _projectService.updateProject(url, vm.projectData).then(function (response) {
                if (response) {

                    showNotification('success', 'Successfully Updated Project');

                }

            })
                .catch(function (err) {
                    showNotification('error', 'Something went wrong');
                });
        }

        //Created by: Patrick Sacay
        //Definition: show notification of successful user creation
        function showNotification(ntype, ntext) {
            var notify = {
                type: ntype,
                title: ntext,
                timeout: 1000 //time in ms
            };

            $scope.$emit('notify', notify);
        }

        /*<---------------- Functions --------------> */

        /*<---------------- Function Calls --------------> */

        initializePage();

        /*<---------------- Function Calls --------------> */







    }]);
