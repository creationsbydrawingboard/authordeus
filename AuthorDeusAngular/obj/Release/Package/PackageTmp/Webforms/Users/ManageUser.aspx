﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageUser.aspx.cs" Inherits="AuthorDeusAngular.WebForm2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script src="../../app/controllers/user/userController.js"></script>
    <script src="../../app/services/userService.js"></script>

    <div ng-controller="UserController as userCtrl" ng-init="userCtrl.getUserList()">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Manage Users</h3>
                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" ng-model="userCtrl.searchText" name="table_search" class="form-control pull-right" placeholder="Search">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>Username</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Contact No</th>
                            <th>Email</th>
                            <th style="width: 10%">Action</th>
                        </tr>
                        <tr ng-repeat="user in  userCtrl.userList | filter: userCtrl.searchText">
                            <td>{{user.Username}}</td>
                            <td>{{user.FirstName}}</td>
                            <td>{{user.LastName}}</td>
                            <td>{{user.ContactNo}}</td>
                            <td>{{user.Email}}</td>
                            <td>
                                <input type="button" class="btn btn-danger" value="delete" ng-click="userCtrl.deleteUser(user)" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <div notifybar style="position: absolute; top: 0; right: 0; z-index: 999999"></div>
    </div>
</asp:Content>
