﻿angular.module('appModule').factory("NotificationService", ['$http', function ($http) {

    var globalConfig = 'contenttype';

    return {

        //since $http.get returns a promise,
        //and promise.then() also returns a promise
        //that resolves to whatever value is returned in it's 
        //callback argument, we can return that.

        //** Dont forget to put forward slash before methods path(see ex[1])
        getNotifications: function (url) {
            //ex[1]
            var method = "/NotificationList";

            url = url + method;

            return $http.get(url).then(function (response) {
                return response.data;
            })
        },

        getNotificationsByUser: function (url, userid) {
            var method = "/GetNotificationsByUser?userId=" + userid;

            url = url + method;

            return $http.get(url).then(function (response) {
                return response.data;
            })
        },

        createNotification: function (url, data) {
            var param = JSON.stringify(data);
            var data = param;

            return $http.post(url, data, globalConfig).then(function (result) {
                return result;
            });
        },

        tagReadNotification: function (url, notification) {

            var param = JSON.stringify(notification);
            var data = param;

            return $http.put(url, data, globalConfig).then(function (result) {
                return result;
            });
        }


    }

}]);