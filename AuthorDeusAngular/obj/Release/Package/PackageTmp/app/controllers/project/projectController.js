﻿'USE STRICT';

angular.module('appModule').controller('ProjectController', ['$scope', '$http', 'BASE_USER_ENDPOINT', '$window', '$timeout', '$sessionStorage', '$ngConfirm', 'ProjectService', 'ProjectGroupService', 'UserService',
    function ($scope, $http, BASE_USER_ENDPOINT, $window, $timeout, $sessionStorage, $ngConfirm, ProjectService, ProjectGroupService, UserService) {

        /*<---------------- Declarations --------------> */

        var vm = this;
        var _projectService = ProjectService;
        var _projectGroupService = ProjectGroupService;
        var _userService = UserService;
        var url = BASE_USER_ENDPOINT + 'Projects';
        var url2 = BASE_USER_ENDPOINT + 'ProjectGroups';
        var userurl = BASE_USER_ENDPOINT + 'Users';

        vm.createProject = createProject;
        vm.myFunction = myFunction;
        vm.editProjectDetails = editProjectDetails;
        vm.manageProject = manageProject;

        vm.projectData = {
            OwnerId: "", Name: "", Description: "", Data: "", DateCreated: "", CreatedBy: "",
            DateUpdated: "", UpdatedBy: "", Void: false
        };

        vm.projectGroupData =
            {
                ProjectId: "", UserId: "", DateCreated: "", CreatedBy: "",
                DateUpdated: "", UpdatedBy: "", Void: false
            };

        vm.projects = [];
        vm.projectsCo = [];
        vm.assigedUsers = [];

        vm.usersToBeRemoved = [];
        vm.usersToBeAssigned = [];  

        vm.currentUser;
        vm.currentProject;

        vm.tempUser;

        /*<---------------- Declarations --------------> */

        /*<---------------- Functions --------------> */

        function initializePage() {
            if ($sessionStorage.SessionData != undefined || $sessionStorage.SessionData != null) {
                vm.currentUser = $sessionStorage.SessionData;
            }
        }

        function createProject() {

            var jc = $ngConfirm({
                title: 'New Project',
                icon: 'fa fa-book',
                content: '<div class="form" name="projectform"><div class="form-group" ng-class="{\'has-error\': inputProjectError}">' +
                '<label>Project Name</label>' +
                '<input type="text" name="inputprojname" class="form-control" ng-model="projectname" placeholder="Enter project name..">' +
                '<span class="help-block" ng-show="inputProjectError">Project name is required</span> </div>' +
                '<div class="form-group">' +
                '<label>Description</label>' +
                '<textarea class="form-control" ng-model="projectdescription" rows="3" placeholder="Enter ..."></textarea> </div>' +
                '<div class="form-group"></div>',
                animation: 'scale',
                columnClass: 'medium',
                buttons: {
                    Create: {
                        btnClass: "btn-blue",
                        action: function (scope) {

                            if (scope.projectname == '' || scope.projectname == undefined) {
                                scope.inputProjectError = true;
                                return false;
                            }
                            else {
                                vm.projectData.Name = scope.projectname;
                                vm.projectData.Description = scope.projectdescription;

                                $ngConfirm({
                                    title: "Confirm",
                                    content: 'Are you sure you want to create this project?',
                                    buttons: {
                                        Yes: {
                                            btnClass: "btn-blue",
                                            action: function () { //// create project record here  
                                                createProjectRecord();
                                                jc.close();
                                            }

                                        },
                                        No: function () { //// cancel creation

                                        }
                                    }
                                });
                                return false;
                            }
                            return false;
                        }
                    },
                    Cancel: {
                        btnClass: "btn-danger",
                    }
                },
                onScopeReady: function (scope) {
                    scope.inputProjectError = false;
                }
            });

        }

        function editProjectDetails(projectParam) {

            vm.projectData.ProjectId = projectParam.ProjectId;
            vm.projectData.Name = projectParam.Name;
            vm.projectData.Description = projectParam.Description;
            vm.projectData.OwnerId = projectParam.OwnerId;
            vm.projectData.DateCreated = projectParam.DateCreated;
            vm.projectData.CreatedBy = projectParam.CreatedBy;

            var jc = $ngConfirm({
                title: 'Update Project',
                icon: 'fa fa-book',
                content: '<div class="form" name="projectform"><div class="form-group" ng-class="{\'has-error\': inputProjectError}">' +
                '<label>Project Name</label>' +
                '<input type="text" name="inputprojname" class="form-control" ng-model="projectname" placeholder="Enter project name..">' +
                '<span class="help-block" ng-show="inputProjectError">Project name is required</span> </div>' +
                '<div class="form-group">' +
                '<label>Description</label>' +
                '<textarea class="form-control" ng-model="projectdescription" rows="3" placeholder="Enter ..."></textarea> </div>' +
                '<div class="form-group"></div>',
                animation: 'scale',
                columnClass: 'medium',
                buttons: {
                    Update: {
                        btnClass: "btn-blue",
                        action: function (scope) {
                            if (scope.projectname == '' || scope.projectname == undefined) {
                                scope.inputProjectError = true;
                                return false;
                            }
                            else {
                                vm.projectData.Name = scope.projectname;
                                vm.projectData.Description = scope.projectdescription;
                                updateProjectRecord(false);
                            }
                        }
                    },
                    Cancel: {
                        btnClass: "btn-default"
                    },
                    Delete: {
                        text: "Delete this project",
                        btnClass: "btn-danger",
                        show: checkDeletePermission(),
                        action: function () {
                            var sjc = $ngConfirm({
                                title: "Confirm",
                                content: '<p>Deleting this project will also delete co-authors access</p>' +
                                '<input type="password" class="form-control" ng-model="deletepassword" ng-change="applychange(deletepassword)" placeholder="Please input your password..">',
                                buttons: {
                                    Yes: {
                                        disabled: true,
                                        text: "I understand",
                                        btnClass: "btn-warning",
                                        action: function () { //// delete project record here
                                            updateProjectRecord(true);
                                            jc.close();
                                        }
                                    },
                                    No: function () {

                                    }
                                },
                                onScopeReady: function (scope) {
                                    scope.applychange = function () {
                                        if (scope.deletepassword == vm.currentUser.Password) {
                                            sjc.buttons.Yes.setDisabled(false);
                                        }
                                    }
                                }
                            });
                            return false;
                        }
                    }
                },
                onScopeReady: function (scope) {
                    scope.inputProjectError = false;
                    scope.projectname = projectParam.Name;
                    scope.projectdescription = projectParam.Description;
                }
            });
        }

        function manageProject(project) {

            vm.currentProject = project;

            _userService.getAssignedUsers(userurl, vm.currentUser.UserId, project.ProjectId).then(function (response) {
                if (response) {
                    vm.assigedUsers = response;
                    openManageProject();
                }
                else {
                    showNotification('error', 'Something went wrong.');
                }
            })
                .catch(function (err) {
                    showNotification('error', 'Something went wrong.');
                });

        }

        function openManageProject() {

            var jc = $ngConfirm({
                title: 'Manage Project',
                icon: 'fa fa-book',
                contentUrl: '/Webforms/Project/confirmhtml/manageproject.html',
                columnClass: 'large',
                buttons:
                {
                    Save:
                    {
                        btnClass: "btn-primary",
                        action: function () {
                            var sjc = $ngConfirm({
                                title: "Confirm",
                                content: 'Are you sure you want to save changes?',
                                buttons: {
                                    Yes: {
                                        btnClass: "btn-warning",
                                        action: function () {
                                            removeAssignedUsers().done(function () {
                                                assignUsers().done(function () {
                                                    showNotification('success', 'Successfully Saved Changes.');
                                                    vm.usersToBeRemoved = [];
                                                    vm.usersToBeAssigned = []; 
                                                    jc.close();
                                                });
                                            });
                                        }
                                    },
                                    No: function () {
                                    }
                                }
                            });
                            return false;
                        }
                    },
                    Cancel:
                    {
                        action: function () {

                        }
                    }
                },
                onScopeReady: function (scope) {

                    scope.userlist = vm.assigedUsers;
                    scope.nonexist = false;
                    scope.emailtext = scope.nonexist == false ? "Find" : "Assign";

                    scope.checkEmail = function (inputemail) {

                        for (var i = 0, iLen = scope.userlist.length; i < iLen; i++) {
                            if (scope.userlist[i].Email == inputemail) {
                                $ngConfirm('Email already assigned in this project.');
                                pass = false;
                                return false;
                            }
                        }

                        if (!scope.nonexist) {

                            if (inputemail != vm.currentUser.Email) {
                                validateEmail(inputemail).done(function (response) {
                                    if (response.data != null) {
                                        scope.nonexist = true;
                                        scope.emailtext = scope.nonexist == false ? "Find" : "Assign";
                                        vm.tempUser = response.data;
                                    }
                                    else {
                                        $ngConfirm('User email is not registered in database.');
                                    }
                                });
                            }
                            else
                            {
                                $ngConfirm('Cannot use own email.');
                            }

                      
                        }
                        else {
                            scope.userlist.push(vm.tempUser);

                            scope.nonexist = false;
                            scope.emailtext = scope.nonexist == false ? "Find" : "Assign";
                            scope.inputemail = "";

                            var dateTime = new Date();

                            var assignData =
                                {
                                    ProjectId: vm.currentProject.ProjectId, UserId: vm.tempUser.UserId,
                                    DateCreated: dateTime, CreatedBy: vm.currentUser.Username,
                                    DateUpdated: dateTime, UpdatedBy: vm.currentUser.Username, Void: false
                                };

                            vm.usersToBeAssigned.push(assignData);
                        }


                    };

                    scope.unassignUser = function (index) {

                        var user = scope.userlist[index];
                        var dateTime = new Date();

                        var unassigData =
                            {
                                ProjectId: vm.currentProject.ProjectId, UserId: user.UserId,
                                DateUpdated: dateTime, UpdatedBy: vm.currentUser.Username, Void: true
                            };

                        vm.usersToBeRemoved.push(unassigData);

                        scope.userlist.splice(index, 1);

                    };
                }
            });

        }

        function validateEmail(emailtext) {

            var deferred = new $.Deferred();

            _userService.verifyEmail(userurl, emailtext).then(function (response) {
                if (response.data != null) {
                    deferred.resolve(response);
                }
                else {
                    deferred.resolve(response);
                }
            })
                .catch(function (err) {
                    showNotification('error', 'Something went wrong.');
                });

            return deferred.promise();

        }

        function removeAssignedUsers() {
            var deferred = new $.Deferred();

            if (vm.usersToBeRemoved.length) {


                _projectGroupService.unassignUserProject(url2, vm.usersToBeRemoved).then(function (response) {
                    if (response) {
                        deferred.resolve(true);
                    }
                    else {
                        deferred.resolve(false);
                    }
                })
                    .catch(function (err) {
                        showNotification('error', 'Something went wrong.');
                    });


            }
            else {
                deferred.resolve(true);
            }

            return deferred.promise();

        }

        function assignUsers() {
            var deferred = new $.Deferred();
            if (vm.usersToBeAssigned.length) {


                _projectGroupService.assignUserProject(url2, vm.usersToBeAssigned).then(function (response) {
                    if (response) {
                        deferred.resolve(true);
                    }
                    else {
                        deferred.resolve(false);
                    }
                })
                    .catch(function (err) {
                        showNotification('error', 'Something went wrong.');
                    });


            }
            else {
                deferred.resolve(true);
            }
            return deferred.promise();
        }

        function checkDeletePermission() {
            if (vm.currentUser.UserTypeId == 1000) //// this will check if user is admin
            {
                return true;
            }
            else {
                if (vm.currentUser.UserId == vm.projectData.OwnerId) //// this will check if current user is owner
                {
                    return true;
                }
            }
            return false;
        };

        function getAuthoredProject() {
            _projectService.getAuthoredProject(url, vm.currentUser.UserId).then(function (data) {
                //this will execute when the AJAX call completes.
                vm.projects = data;
            });
        }

        function getCoAuthoredProject() {
            _projectService.getCoAuthoredProject(url, vm.currentUser.UserId).then(function (data) {
                //this will execute when the AJAX call completes.
                vm.projectsCo = data;
            });
        }

        function myFunction() {
            var x = Math.floor((Math.random() * 100) + 1);
            return x + '%';
        }

        function createProjectRecord() {
            var dateTime = new Date();

            vm.projectData.OwnerId = vm.currentUser.UserId;
            vm.projectData.DateCreated = dateTime;
            vm.projectData.DateUpdated = dateTime;
            vm.projectData.CreatedBy = vm.currentUser.Username; //change this to session name
            vm.projectData.UpdatedBy = vm.currentUser.Username;
            vm.projectData.Void = false;

            _projectService.createProject(url, vm.projectData).then(function (response) {
                if (response) {
                    vm.projectGroupData.ProjectId = response.data.ProjectId;
                    createProjectGroupRecord();
                }

            })
                .catch(function (err) {
                    showNotification('error', 'Something went wrong');
                });
        }

        function updateProjectRecord(status) {

            var dateTime = new Date();

            vm.projectData.DateUpdated = dateTime;
            vm.projectData.UpdatedBy = vm.currentUser.Username;
            vm.projectData.Void = status;

            _projectService.updateProject(url, vm.projectData).then(function (response) {
                if (response) {
                    if (status) {
                        showNotification('warning', 'Successfully delete project');
                    }
                    else {
                        showNotification('success', 'Successfully Updated Project');
                    }

                    getAuthoredProject();
                    getCoAuthoredProject();

                }

            })
                .catch(function (err) {
                    showNotification('error', 'Something went wrong');
                });
        }

        function createProjectGroupRecord() {
            var dateTime = new Date();

            vm.projectGroupData.UserId = vm.currentUser.UserId;
            vm.projectGroupData.DateCreated = dateTime;
            vm.projectGroupData.DateUpdated = dateTime;
            vm.projectGroupData.CreatedBy = vm.currentUser.Username; //change this to session name
            vm.projectGroupData.UpdatedBy = vm.currentUser.Username;
            vm.projectGroupData.Void = false;

            _projectGroupService.createProjectGroup(url2, vm.projectGroupData).then(function (response) {
                if (response) {
                    showNotification('success', 'Successfully Created Project');
                    getAuthoredProject();
                    getCoAuthoredProject();
                }
            })
                .catch(function (err) {
                    showNotification('error', 'Something went wrong');
                });
        }

        //Created by: Patrick Sacay
        //Definition: show notification of successful user creation
        function showNotification(ntype, ntext) {
            var notify = {
                type: ntype,
                title: ntext,
                timeout: 1000 //time in ms
            };

            $scope.$emit('notify', notify);
        }

        /*<---------------- Functions --------------> */

        /*<---------------- Function Calls --------------> */
        initializePage();
        getAuthoredProject();
        getCoAuthoredProject();
        /*<---------------- Function Calls --------------> */

    }]);