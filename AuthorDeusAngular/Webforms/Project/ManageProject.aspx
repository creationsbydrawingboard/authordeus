﻿<%@ Page Title="Manage Project" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageProject.aspx.cs" Inherits="AuthorDeusAngular.WebForm3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script src="../../app/services/userService.js"></script>
    <script src="../../app/services/projectService.js"></script>
    <script src="../../app/services/projectGroupService.js"></script>
    <script src="../../app/services/notificationService.js"></script>
    <script src="../../app/controllers/project/projectController.js"></script>

    <style>
        .margin-bot-10px {
            margin-bottom: 10px;
        }
    </style>

    <div ng-controller="ProjectController as projCtrl">
        <div notifybar style="position: absolute; top: 0; right: 0; z-index: 99999999999999999"></div>
        <div class="row margin-bot-10px">
            <div class="col-lg-2 col-md-2">
                <input type="button" value="Create Project" class="btn btn-primary" ng-click="projCtrl.createProject()"></input>
            </div>
        </div>
        <%-- show author view--%>
        <div ng-show="projCtrl.currentUser.UserTypeId == 1000">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Authored Projects</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th style="width: 5%">#</th>
                                <th style="width: 25%">Project Title</th>
                                <th style="width: 35%">Description</th>
                                <th style="width: 10%">Status</th>
                                <th style="width: 25%; text-align: center">Manage</th>
                            </tr>
                            <tr ng-repeat="project in projCtrl.projects">
                                <td>{{$index + 1}}</td>
                                <td><a href="#">
                                    <label ng-click="projCtrl.editProjectContent(project)">{{project.Name}}</label>
                                </a></td>
                                <td>{{project.Description || 'N/A'}}</td>
                                <td>
                                    <div ng-if="project.Status">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default"><span class="badge bg-yellow">3</span>  Enquiry</button>
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#" ng-click="projCtrl.quoteInvitation(project)">Request Quote</a></li>
                                                <li><a href="#">View Quotes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div ng-if="!project.Status">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default" disabled>No Quote</button>
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#" ng-click="projCtrl.quoteInvitation(project)">Request Quote</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                                <td style="text-align: center">
                                    <div class="btn-group">
                                        <button type="button" class="btn bg-default" ng-click="projCtrl.editProjectDetails(project)"><i class="fa fa-align-left"></i>&nbsp;&nbsp;Details </button>
                                        <button type="button" class="btn bg-navy" ng-click="projCtrl.manageProject(project)"><i class="fa  fa-file"></i>&nbsp;&nbsp;Manage</button>
                                        <button type="button" class="btn btn-danger" ng-click="projCtrl.deleteProject(project)"><i class="fa  fa-remove"></i>&nbsp;&nbsp;Delete</button>
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <%-- show author view--%>
        <div ng-show="projCtrl.currentUser.UserTypeId == 1002">

            <%--   <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Authored</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <div ng-repeat="project in projCtrl.projects" ng-if="$index % 4 == 0" class="row">
                        <div ng-repeat="item in projCtrl.projects.slice($parent.$index, ($parent.$index + 4))" class="col-xs-3">
                            <div class="box box-primary">
                                <div class="box-body box-profile">
                                    <img class="profile-user-img img-responsive img-circle" src="../../Sources/book.png" runat="server" alt="User profile picture">
                                    <a href="#">
                                        <h3 class="profile-username text-center" ng-click="projCtrl.editProjectDetails(item)">{{item.Name}}</h3>
                                    </a>
                                    <p class="text-muted text-center">{{item.Description || 'N/A'}}</p>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a href="#" class="btn btn-success btn-block"><b>Details</b></a>  <a href="#" class="btn btn-primary btn-block" ng-click="projCtrl.manageProject(item)"><b>Manage</b></a>
                                        </div>

                                    </div>

                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>

                </div>
            </div>--%>

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Authored Projects</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th style="width: 5%">#</th>
                                <th style="width: 25%">Project Title</th>
                                <th style="width: 35%">Description</th>
                                <th style="width: 10%">Status</th>
                                <th style="width: 25%; text-align: center">Manage</th>
                            </tr>
                            <tr ng-repeat="project in projCtrl.projects">
                                <td>{{$index + 1}}</td>
                                <td><a href="#">
                                    <label ng-click="projCtrl.editProjectContent(project)">{{project.Name}}</label>
                                </a></td>
                                <td>{{project.Description || 'N/A'}}</td>
                                <td>
                                    <div ng-if="project.Status">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default"><span class="badge bg-yellow">3</span>  Enquiry</button>
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#" ng-click="projCtrl.quoteInvitation(project)">Request Quote</a></li>
                                                <li><a href="#">View Quotes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div ng-if="!project.Status">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default">No Quote</button>
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#" ng-click="projCtrl.quoteInvitation(project)">Request Quote</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                                <td style="text-align: center">
                                    <div class="btn-group">
                                        <button type="button" class="btn bg-default" ng-click="projCtrl.editProjectDetails(project)"><i class="fa fa-align-left"></i>&nbsp;&nbsp;Details </button>
                                        <button type="button" class="btn bg-navy" ng-click="projCtrl.manageProject(project)"><i class="fa  fa-file"></i>&nbsp;&nbsp;Manage</button>
                                        <button type="button" class="btn btn-danger" ng-click="projCtrl.deleteProject(project)"><i class="fa  fa-remove"></i>&nbsp;&nbsp;Delete</button>
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>


    </div>
</asp:Content>
