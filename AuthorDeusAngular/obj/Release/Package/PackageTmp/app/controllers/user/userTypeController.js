﻿'USE STRICT'

angular.module('appModule').controller('UserTypeController', ['$scope', '$http', 'UserTypeService', 'BASE_USER_ENDPOINT', function ($scope, $http, UserTypeService, BASE_USER_ENDPOINT) {


    /*<---------------- Declarations --------------> */

    var vm = this;
    var _userTypeService = UserTypeService;
    var url = BASE_USER_ENDPOINT + 'UserTypes';

    vm.getUserTypes = getUserTypes;
    vm.userTypes = [];


    /*<---------------- Declarations --------------> */

    /*<---------------- Functions --------------> */

    function getUserTypes() {

        _userTypeService.getUserTypes(url).then(function (data) {
            if (data) {
                vm.userTypes = data;

                $scope.userCtrl.selectedUserType = data[0];

            }
        })
            .catch(function (error) {
                //put error handler here
            });

    };

    /*<---------------- Functions --------------> */

    /*<---------------- Function Calls --------------> */

    getUserTypes();

    /*<---------------- Function Calls --------------> */

}]);