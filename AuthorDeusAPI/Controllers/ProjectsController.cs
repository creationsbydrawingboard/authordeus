﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AuthorDeusData;
using System.Runtime.Serialization;
using AuthorDeusData.Models;

namespace AuthorDeusAPI.Controllers
{
  public class ProjectsController : ApiController
  {
    private AuthorDeusEntities db = new AuthorDeusEntities();

    public ProjectsController()
    {
      db.Configuration.ProxyCreationEnabled = false;
    }

    /// <summary>
    /// Load projects from database
    /// </summary>
    /// <returns></returns>
    // GET: api/Projects
    public IQueryable<Project> GetProject()
    {
      return db.Project;
    }

    /// <summary>
    /// Get project record by id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    // GET: api/Projects/5
    [ResponseType(typeof(Project))]
    public async Task<IHttpActionResult> GetProject(int id)
    {
      Project project = await db.Project.FindAsync(id);
      if (project == null)
      {
        return NotFound();
      }

      return Ok(project);
    }

    /// <summary>
    /// update project record in the database
    /// </summary>
    /// <param name="project"></param>
    /// <returns></returns>
    // PUT: api/Projects/5
    [ResponseType(typeof(void))]
    public async Task<IHttpActionResult> PutProject(Project project)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      //if (id != project.ProjectId)
      //{
      //  return BadRequest();
      //}

      db.Entry(project).State = EntityState.Modified;

      try
      {
        await db.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!ProjectExists(project.ProjectId))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return StatusCode(HttpStatusCode.NoContent);
    }

    /// <summary>
    /// create project record in the database
    /// </summary>
    /// <param name="project"></param>
    /// <returns></returns>
    // POST: api/Projects
    [ResponseType(typeof(Project))]
    public async Task<IHttpActionResult> PostProject(Project project)
    {
      try
      {
        if (!ModelState.IsValid)
        {
          return BadRequest(ModelState);
        }

        db.Project.Add(project);
        await db.SaveChangesAsync();

      }
      catch (Exception)
      {
        return BadRequest();
        throw;
      }

      return Ok(project);

      ////return CreatedAtRoute("DefaultApi", new { id = project.ProjectId }, project);
    }

    /// <summary>
    /// hard delete project record in the database
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    // DELETE: api/Projects/5
    [ResponseType(typeof(Project))]
    public async Task<IHttpActionResult> DeleteProject(int id)
    {
      Project project = await db.Project.FindAsync(id);
      if (project == null)
      {
        return NotFound();
      }

      db.Project.Remove(project);
      await db.SaveChangesAsync();

      return Ok(project);
    }

    /// <summary>
    /// load project list from database
    /// </summary>
    /// <returns></returns>
    // GET: api/Project/ProjectList
    [ResponseType(typeof(List<Project>))]
    [Route("api/Projects/ProjectList")]
    public async Task<IHttpActionResult> GetUserList()
    {
      List<Project> projectList = await (from p in db.Project
                                         where p.Void != true
                                         select p).ToListAsync();
      return Ok(projectList);
    }

    /// <summary>
    /// load all projects created by userId
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    // GET: api/ProjectGroups/GetAuthoredProject
    [ResponseType(typeof(List<Project>))]
    [Route("api/Projects/GetAuthoredProject")]
    public async Task<IHttpActionResult> GetAuthoredProject(int userId)
    {
      List<Project> projectList = await (from p in db.Project
                                         join q in db.ProjectGroup
                                         on p.OwnerId equals q.UserId
                                         where p.Void != true && q.UserId == userId
                                         select p).Distinct().ToListAsync();
      return Ok(projectList);
    }

    /// <summary>
    /// load all projects assigned to the user
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    // GET: api/ProjectGroups/GetAuthoredProject
    [ResponseType(typeof(List<Project>))]
    [Route("api/Projects/GetCoAuthoredProject")]
    public async Task<IHttpActionResult> GetCoAuthoredProject(int userId)
    {
      List<Project> projectList = await (from p in db.Project
                                         join q in db.ProjectGroup
                                         on p.ProjectId equals q.ProjectId
                                         where p.Void != true && p.OwnerId != userId && q.Void != true && q.UserId == userId
                                         select p).ToListAsync();

      return Ok(projectList);
    }


    // PUT: api/Projects/CreateInvitations
    [ResponseType(typeof(void))]
    [HttpPost]
    [Route("api/Projects/CreateInvitations")]
    public async Task<IHttpActionResult> CreateInvitations(List<QuoteInvite> quoteInviteList)
    {
      try
      {

        foreach (QuoteInvite quote in quoteInviteList)
        {

          db.QuoteInvite.Add(quote);
          
          await db.SaveChangesAsync();
         
         
        }

      }
      catch (DbUpdateConcurrencyException)
      {
        return NotFound();
      }

      return StatusCode(HttpStatusCode.NoContent);
    }


    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        db.Dispose();
      }
      base.Dispose(disposing);
    }

    private bool ProjectExists(int id)
    {
      return db.Project.Count(e => e.ProjectId == id) > 0;
    }
  }
}