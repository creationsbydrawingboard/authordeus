﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="AuthorDeus.Login" %>

<!DOCTYPE html>


<script src="Scripts/jquery-1.10.2.min.js"></script>
<script src="Scripts/bootstrap.js"></script>
<script src="Scripts/angular/angular.min.js"></script>
<script src="Scripts/angular/angular-route.min.js"></script>
<script src="Scripts/angular/angular-notify.min.js"></script>
<script src="Scripts/angular/ngStorage.min.js"></script>
<script src="Scripts/angular/ng-table.min.js"></script>
<script src="Scripts/angular/angular-confirm.min.js"></script>
<script src="Scripts/angular/angular-summernote.min.js"></script>
<script src="Scripts/summernote/summernote.min.js"></script>
<script src="Scripts/angular/angular-filter.min.js"></script>

<script src="app/app.js"></script>

<script src="app/services/userService.js"></script>

<script src="app/controllers/user/loginController.js"></script>

<link href="Content/bootstrap.min.css" rel="stylesheet" />
<link href="Content/Site.css" rel="stylesheet" />
<link href="Content/font-awesome.min.css" rel="stylesheet" />
<link href="Content/AdminLTE/css/AdminLTE.min.css" rel="stylesheet" />
<link href="Content/AngularNotify/angular-notify-material.min.css" rel="stylesheet" />
<link href="Scripts/summernote/summernote.css" rel="stylesheet" />

<html xmlns="http://www.w3.org/1999/xhtml" ng-app="appModule" ng-controller="LoginController as login">
<head runat="server">
    <title></title>

    <style>
        .hrstyle {
            margin: 15px 0px 15px 0px;
            border-top: 1px solid #ddd !important;
        }
    </style>
</head>

<body style="background-image: url('Sources/Images/wallhaven-405832.jpg'); background-size: cover">
    <div id="myOverlay" ng-if="login.loader"></div>
    <div id="loadingGIF">
        <img src="Sources/bookjeep.gif" ng-if="login.loader" />
    </div>
    <form name="loginform" novalidate>
        <div class="container-fluid">
            <div class="row center_div">
                <div class="col-md-3 col-sm-10 col-xs-10 col-md-offset-7 col-sm-offset-1 col-xs-offset-1 well">
                  <%--  <div class="form-group social-buttons">
                        <div class="row">
                            <div class="col-md-12">
                                <a class="btn btn-block btn-social btn-facebook">
                                    <i class="fa fa-facebook"></i>Sign in with Facebook
                                </a>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-md-12">
                                or
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a class="btn btn-block btn-social btn-google">
                                    <i class="fa fa-google"></i>Sign in with Google
                                </a>
                            </div>
                        </div>
                    </div>--%>
                    <hr />
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input ng-model="login.user.Username" ng-required="true" name="inputusername" type="text" class="form-control" id="inputusername" aria-describedby="emailHelp" placeholder="Enter username">
                        <small id="usernamehelp" class="form-text text-muted">We'll never share your username with anyone else.</small>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword">Password</label>
                        <input ng-model="login.user.Password" ng-required="true" name="inputpassword" type="password" class="form-control" id="inputPassword" placeholder="Password">
                    </div>
                    <button type="button" class="btn btn-primary" ng-click="login.verifyUserCredential()" ng-disabled="!loginform.$valid">Login</button>
                    <hr class="hrstyle">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <p>
                                <a href="Webforms/Registration">Not yet a member? Register here.</a>
                            </p>
                        </div>
                    </div>
                    <div class="callout callout-danger" ng-show="loginform.inputusername.$invalid && !loginform.inputusername.$pristine">
                        You username is required.
                    </div>
                    <div class="callout callout-danger" ng-show="loginform.inputpassword.$invalid && !loginform.inputpassword.$pristine">
                        Your password is required.
                    </div>
                      <div class="callout callout-danger" ng-show="login.errorMessage">
                        Invalid log in username/password.
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
