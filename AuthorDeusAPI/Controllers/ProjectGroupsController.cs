﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AuthorDeusData;
using AuthorDeusData.Models;

namespace AuthorDeusAPI.Controllers
{
  public class ProjectGroupsController : ApiController
  {
    

    private AuthorDeusEntities db = new AuthorDeusEntities();

    // GET: api/ProjectGroups
    public IQueryable<ProjectGroup> GetProjectGroup()
    {
      return db.ProjectGroup;
    }

    // GET: api/ProjectGroups/5
    [ResponseType(typeof(ProjectGroup))]
    public async Task<IHttpActionResult> GetProjectGroup(int id)
    {
      ProjectGroup projectGroup = await db.ProjectGroup.FindAsync(id);
      if (projectGroup == null)
      {
        return NotFound();
      }

      return Ok(projectGroup);
    }

    // PUT: api/ProjectGroups/5
    [ResponseType(typeof(void))]
    public async Task<IHttpActionResult> PutProjectGroup(int id, ProjectGroup projectGroup)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id != projectGroup.ProjectGroupId)
      {
        return BadRequest();
      }

      db.Entry(projectGroup).State = EntityState.Modified;

      try
      {
        await db.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!ProjectGroupExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return StatusCode(HttpStatusCode.NoContent);
    }

    // PUT: api/ProjectGroups/5
    [ResponseType(typeof(void))]
    [HttpPost]
    [Route("api/ProjectGroups/UnassignUserProject")]
    public async Task<IHttpActionResult> UnassignUserProject(List<ProjectGroupDTO> projectGroupList)
    {
      try
      {

        foreach (ProjectGroupDTO proj in projectGroupList)
        {

          var resp = await (from p in db.ProjectGroup
                            where p.ProjectId == proj.ProjectId && p.UserId == proj.UserId && p.Void == false
                            select p).FirstOrDefaultAsync();

          resp.UpdatedBy = proj.UpdatedBy;
          resp.DateUpdated = proj.DateUpdated;
          resp.Void = proj.Void;

          await db.SaveChangesAsync();

        }

      }
      catch (DbUpdateConcurrencyException)
      {
        return NotFound();
      }

      return StatusCode(HttpStatusCode.NoContent);
    }

    // PUT: api/ProjectGroups/5
    [ResponseType(typeof(void))]
    [HttpPost]
    [Route("api/ProjectGroups/AssignUserProject")]
    public async Task<IHttpActionResult> AssignUserProject(List<ProjectGroup> projectGroupList)
    {
      try
      {

        foreach (ProjectGroup proj in projectGroupList)
        {

          db.ProjectGroup.Add(proj);
          
          await db.SaveChangesAsync();

        }

      }
      catch (DbUpdateConcurrencyException)
      {
        return NotFound();
      }

      return StatusCode(HttpStatusCode.NoContent);
    }

    // POST: api/ProjectGroups
    [ResponseType(typeof(ProjectGroup))]
    public async Task<IHttpActionResult> PostProjectGroup(ProjectGroup projectGroup)
    {
      try
      {
        if (!ModelState.IsValid)
        {
          return BadRequest(ModelState);
        }

        db.ProjectGroup.Add(projectGroup);
        await db.SaveChangesAsync();
      }
      catch (Exception)
      {
        return BadRequest();
        throw;
      }

      return Ok(projectGroup);
    }

    // DELETE: api/ProjectGroups/5
    [ResponseType(typeof(ProjectGroup))]
    public async Task<IHttpActionResult> DeleteProjectGroup(int id)
    {
      ProjectGroup projectGroup = await db.ProjectGroup.FindAsync(id);
      if (projectGroup == null)
      {
        return NotFound();
      }

      db.ProjectGroup.Remove(projectGroup);
      await db.SaveChangesAsync();

      return Ok(projectGroup);
    }


    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        db.Dispose();
      }
      base.Dispose(disposing);
    }

    private bool ProjectGroupExists(int id)
    {
      return db.ProjectGroup.Count(e => e.ProjectGroupId == id) > 0;
    }

  }
}