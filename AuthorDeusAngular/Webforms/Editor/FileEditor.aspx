﻿<%@ Page Title="Editor" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FileEditor.aspx.cs" Inherits="AuthorDeusAngular.WebForm5" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
            }
        }

        .note-editor.note-frame.note-document {
            display: block !important;
            overflow: none !important;
        }

        /*.note-editor.note-frame.note-document .note-editing-area {
                background-color: #fafafa !important;
                overflow: auto !important;
            }*/

        /*.note-editor.note-frame .note-editing-area .note-editable {
            overflow: auto !important;
            border-radius: 0 !important;
            box-shadow: 0 !important;
            width: 100% !important;
        }*/

        /*.note-editor.note-frame.note-document .note-editing-area.a3:after {
            content: "842 x 1191"
        }*/

        /*.note-editor.note-frame.note-document .note-editing-area .note-editable {
            display: block !important;
            margin: 40px auto 2px auto !important;
            overflow: hidden !important;
            overflow-y: auto !important;
            border: 1px solid #d3d3d3 !important;
            border-radius: 5px !important;
            box-shadow: 0 0 5px rgba(0,0,0,.1) !important;
        }*/

        .note-editable {
            width: 595px !important;
            zoom: 130% !important;
            padding: 35px !important;
        }

        /*page[size="A4"] {
            width: 21cm;
            height: 29.7cm;
        }

            page[size="A4"][layout="portrait"] {
                width: 29.7cm;
                height: 21cm;
            }*/

        .note-editing-area {
            background-color: #fafafa !important;
            overflow: auto !important;
        }

        .note-editable {
            overflow: auto !important;
            border-radius: 0 !important;
            box-shadow: 0 !important;
        }

        .note-editable {
            display: block !important;
            margin: 40px auto 2px auto !important;
            overflow: hidden !important;
            overflow-y: auto !important;
            border: 1px solid #d3d3d3 !important;
            border-radius: 5px !important;
            box-shadow: 0 0 5px rgba(0,0,0,.1) !important;
        }
    </style>

    <script src="../../app/services/projectService.js"></script>
    <script src="../../app/controllers/project/editorController.js"></script>

    <div ng-controller="EditorController as editCtrl">
        <div notifybar style="position: absolute; top: 0; right: 0; z-index: 99999999999999999"></div>
        <div class="row">
            <div class="col-md-4">
                <button type="button" class="btn btn-success" ng-click="editCtrl.updateProjectRecord()">Save Changes</button>

            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <summernote config="editCtrl.options" ng-model="editCtrl.projectContent"></summernote>
            </div>
        </div>
    </div>


</asp:Content>
