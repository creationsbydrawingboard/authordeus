﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.UI;

namespace AuthorDeusAngular
{
  public class BundleConfig
  {
    // For more information on Bundling, visit https://go.microsoft.com/fwlink/?LinkID=303951
    public static void RegisterBundles(BundleCollection bundles)
    {
      ////Bundle for basic scripts
      bundles.Add(new ScriptBundle("~/bundles/scriptsBundle").Include(
                      "~/Scripts/jquery-1.10.2.min.js",
                      "~/Scripts/bootstrap.js",
                      "~/Content/AdminLTE/js/adminlte.js"));

      ////Bundle for angular scripts
      bundles.Add(new ScriptBundle("~/bundles/angularBundle").Include(
                      "~/Scripts/angular/angular.min.js",
                      "~/Scripts/angular/angular-route.min.js",
                      "~/Scripts/angular/angular-notify.min.js",
                      "~/Scripts/angular/ngStorage.min.js",
                      "~/Scripts/angular/ng-table.min.js",
                      "~/Scripts/angular/angular-confirm.min.js",
                      "~/Scripts/angular/angular-summernote.min.js",
                      "~/Scripts/angular/angular-filter.min.js"));

      ////Bundle for summernote scripts
      bundles.Add(new ScriptBundle("~/bundles/summernoteBundle").Include(
                       "~/Scripts/summernote/summernote.js",
                       "~/Scripts/summernote/summernote-ext-print.js",
                       "~/Scripts/summernote/summernote-templates/summernote-templates.js"));



      ////Bundle for styling
      bundles.Add(new StyleBundle("~/bundles/styleBundle").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/AdminLTE/css/AdminLTE.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/AdminLTE/css/skins/skin-yellow-light.min.css",
                      "~/Content/AngularNotify/angular-notify-material.min.css",
                      "~/Content/materialicon.css",
                      "~/Content/angular-confirm.css",
                      "~/Scripts/summernote/summernote.css"));
    }
  }
}