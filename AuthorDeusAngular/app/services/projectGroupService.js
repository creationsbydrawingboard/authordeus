﻿angular.module('appModule').factory("ProjectGroupService", ['$http', function ($http) {

    var globalConfig = 'contenttype';

    return {

        //since $http.get returns a promise,
        //and promise.then() also returns a promise
        //that resolves to whatever value is returned in it's 
        //callback argument, we can return that.

        //** Dont forget to put forward slash before methods path(see ex[1])
        getProjectGroup: function (url) {
            //ex[1]
            var method = "/ProjectList";

            url = url + method;

            return $http.get(url).then(function (response) {
                return response.data;
            });
        },

        createProjectGroup: function (url, projectgroup) {            

            var param = JSON.stringify(projectgroup);
            var data = param;         

            return $http.post(url, data, globalConfig).then(function (result) {
                return result;
            });
        },

        unassignUserProject: function (url, projectgroup) {

            var param = JSON.stringify(projectgroup);
            var data = param;              
            var method = "/UnassignUserProject";

            url = url + method;

            return $http.post(url, data, globalConfig).then(function (response) {
                return response.data;
            })
        },

        assignUserProject: function (url, projectgroup) {

            var param = JSON.stringify(projectgroup);
            var data = param;
            var method = "/AssignUserProject";

            url = url + method;

            return $http.post(url, data, globalConfig).then(function (response) {
                return response.data;
            })
        }

    }

}]);