﻿'USE STRICT'

angular.module('appModule').controller('HomeController', ['$scope', '$sessionStorage', 'BASE_USER_ENDPOINT', '$window', 'NotificationService', function ($scope, $sessionStorage, BASE_USER_ENDPOINT, $window, NotificationService) {


    /*<---------------- Declarations --------------> */

    var vm = this;
    var _notificationService = NotificationService;

    var notificationurl = BASE_USER_ENDPOINT + 'Notifications';

    vm.logout = logout;


    vm.currentUser = $sessionStorage.SessionData;
    vm.notificationList;

    //vm.Firstname = vm.currentUser.FirstName;
    //vm.UserTypeDescription = vm.currentUser.UserTypeDescription;

    /*<---------------- Declarations --------------> */

    /*<---------------- Functions --------------> */

    //Definition: initialize page using project controller
    function initializePage() {
        if ($sessionStorage.SessionData != undefined || $sessionStorage.SessionData != null) {
            vm.currentUser = $sessionStorage.SessionData;
        
        }
    }


    function logout() {
        $sessionStorage.$reset();
        $window.location.href = '../../Login.aspx';
    };

    //Created by: Patrick Sacay
    //Definition: show notification of successful user creation
    function showNotification(ntype, ntext) {
        var notify = {
            type: ntype,
            title: ntext,
            timeout: 1000 //time in ms
        };

        $scope.$emit('notify', notify);
    }

    /*<---------------- Functions --------------> */

    /*<---------------- Function Calls --------------> */

    initializePage();

    /*<---------------- Function Calls -------------->*/


    //alert($sessionStorage.SessionData.UserId);




}]);