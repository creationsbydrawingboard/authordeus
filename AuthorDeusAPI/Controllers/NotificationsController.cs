﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AuthorDeusData;

namespace AuthorDeusAPI.Controllers
{
  public class NotificationsController : ApiController
  {
    private AuthorDeusEntities db = new AuthorDeusEntities();

    // GET: api/Notifications
    public IQueryable<Notification> GetNotification()
    {
      return db.Notification;
    }

    // GET: api/Notifications/5
    [ResponseType(typeof(Notification))]
    public async Task<IHttpActionResult> GetNotification(long id)
    {
      Notification notification = await db.Notification.FindAsync(id);
      if (notification == null)
      {
        return NotFound();
      }

      return Ok(notification);
    }

    // GET: api/Notifications/5
    [ResponseType(typeof(List<Notification>))]
    [Route("api/Notifications/GetNotificationsByUser")]
    public async Task<IHttpActionResult> GetNotificationsByUser(int userId)
    {

      List<Notification> notificationList = new List<Notification>();

      notificationList = await (from p in db.Notification
                                where p.ReceiverId == userId && p.Void == false
                                select p).ToListAsync();

      if (notificationList == null)
      {
        return NotFound();
      }

      return Ok(notificationList);
    }

    // PUT: api/Notifications/5
    [ResponseType(typeof(void))]
    public async Task<IHttpActionResult> PutNotification(Notification notification)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      //if (id != notification.NotificationId)
      //{
      //  return BadRequest();
      //}

      db.Entry(notification).State = EntityState.Modified;

      try
      {
        await db.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        //if (!NotificationExists(id))
        //{
        //  return NotFound();
        //}
        //else
        //{
        //  throw;
        //}
      }

      return StatusCode(HttpStatusCode.NoContent);
    }

    // POST: api/Notifications
    [ResponseType(typeof(Notification))]
    public async Task<IHttpActionResult> PostNotification(Notification notification)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      db.Notification.Add(notification);
      await db.SaveChangesAsync();

      return CreatedAtRoute("DefaultApi", new { id = notification.NotificationId }, notification);
    }

    // DELETE: api/Notifications/5
    [ResponseType(typeof(Notification))]
    public async Task<IHttpActionResult> DeleteNotification(long id)
    {
      Notification notification = await db.Notification.FindAsync(id);
      if (notification == null)
      {
        return NotFound();
      }

      db.Notification.Remove(notification);
      await db.SaveChangesAsync();

      return Ok(notification);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        db.Dispose();
      }
      base.Dispose(disposing);
    }

    private bool NotificationExists(long id)
    {
      return db.Notification.Count(e => e.NotificationId == id) > 0;
    }
  }
}