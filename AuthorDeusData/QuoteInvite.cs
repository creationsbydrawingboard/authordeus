//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuthorDeusData
{
    using System;
    using System.Collections.Generic;
    
    public partial class QuoteInvite
    {
        public int InviteId { get; set; }
        public Nullable<int> ProjectId { get; set; }
        public Nullable<int> PrinterId { get; set; }
        public string Message { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> DateUpdated { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<bool> IsRead { get; set; }
        public Nullable<bool> Void { get; set; }
    
        public virtual Project Project { get; set; }
    }
}
