﻿'USE STRICT';

angular.module('appModule').controller('LoginController', ['$scope', '$http', 'UserService', '$window', 'BASE_USER_ENDPOINT', '$location', '$sessionStorage',
    function ($scope, $http, UserService, $window, BASE_USER_ENDPOINT, $location, $sessionStorage) {

        /*<---------------- Declarations --------------> */

        var vm = this;
        var _userService = UserService;
        var url = BASE_USER_ENDPOINT + 'Users';

        vm.verifyUserCredential = verifyUserCredential;
        vm.verifyInput = verifyInput;

        vm.user = { Username: "", Password: "" };
        vm.loader = false;
        vm.errorMessage = false;

        /*<---------------- Declarations --------------> */

        /*<---------------- Functions --------------> */

        //Created by: Patrick Sacay
        //Definition: Verify user log in credentials
        function verifyUserCredential() {

            //$location.url($location.path('Default').search('id=1000'));

            vm.loader = true;
            _userService.verifyUser(url,vm.user).then(function (response) {
                if (response) {
                    vm.loader = false;

                    $sessionStorage.SessionData = response.data;
                    //$window.location.href = '../../../FileEditor.aspx';
                    $window.location.href = '../../../Default.aspx';
                }
                else
                {

                }
            })
                .catch(function (err) {
                    vm.loader = false;
                    vm.errorMessage = true;
                });
        };

        //Created by: Patrick Sacay
        //Definition: Verify user log in credentials
        function verifyInput() {

            if ($scope.loginform.$valid) {
                alert('all inputs are valid ');
            }
            else {
                alert('all inputs are not valid ');
            }

            //if (!$scope.loginform.user.$valid) {
            //    vm.errorMessage = "error";
            //}
        }

         //Created by: Patrick Sacay
        //Definition: check if user has session
        function redirectIfHasSession()
        {
            if ($sessionStorage.SessionData != undefined || $sessionStorage.SessionData != null)
            {                
               $window.location.href = '../../../Default.aspx';
            }

        }


        /*<---------------- Functions --------------> */

        /*<---------------- Function Calls --------------> */

        redirectIfHasSession();

        /*<---------------- Function Calls --------------> */

    }]);