﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AuthorDeusAPI
{
  public static class WebApiConfig
  {
    public static void Register(HttpConfiguration config)
    {
      // Web API configuration and services

      // Web API routes
      config.MapHttpAttributeRoutes();

      var cors = new EnableCorsAttribute("*", "*", "*");
      config.EnableCors(cors);

      config.Routes.MapHttpRoute(
          name: "DefaultApi",
          routeTemplate: "api/{controller}/{id}",
          defaults: new { id = RouteParameter.Optional }
      );

      //var json = config.Formatters.JsonFormatter;
      //json.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
      //config.Formatters.Remove(config.Formatters.XmlFormatter);

      //var serializerSettings = new JsonSerializerSettings()
      //{
      //  ReferenceLoopHandling = ReferenceLoopHandling.Ignore
      //};
      //config.Formatters.JsonFormatter.SerializerSettings = serializerSettings;


      //var json = config.Formatters.JsonFormatter;
      //json.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
      //config.Formatters.Remove(config.Formatters.XmlFormatter);

      config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
      config.Formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.None;

    }
  }
}
