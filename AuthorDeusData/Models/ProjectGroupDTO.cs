﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthorDeusData.Models
{
  public class ProjectGroupDTO
  {

    public int ProjectGroupId { get; set; }
    public Nullable<int> ProjectId { get; set; }
    public Nullable<int> UserId { get; set; }
    public Nullable<System.DateTime> DateCreated { get; set; }
    public string CreatedBy { get; set; }
    public Nullable<System.DateTime> DateUpdated { get; set; }
    public string UpdatedBy { get; set; }
    public Nullable<bool> Void { get; set; }

    public virtual Project Project { get; set; }
    public virtual User User { get; set; }

  }
}