﻿angular.module('appModule').factory("ProjectService", ['$http', function ($http) {

    var globalConfig = 'contenttype';

    return {

        //since $http.get returns a promise,
        //and promise.then() also returns a promise
        //that resolves to whatever value is returned in it's 
        //callback argument, we can return that.

        //** Dont forget to put forward slash before methods path(see ex[1])
        getProjects: function (url)
        {
            //ex[1]
            var method = "/ProjectList";

            url = url + method;

            return $http.get(url).then(function (response)
            {
                return response.data;
            })
        },

        getAuthoredProject: function (url, userid) {
            //ex[1]
            var method = "/GetAuthoredProject?userId=" + userid;
            
            url = url + method;

            return $http.get(url).then(function (response) {
                return response.data;
            })
        },

        getCoAuthoredProject: function (url, userid) {
            //ex[1]
            var method = "/GetCoAuthoredProject?userId=" + userid;

            url = url + method;

            return $http.get(url).then(function (response) {
                return response.data;
            })
        },

        createProject: function (url, project) {
            var param = JSON.stringify(project);
            var data = param;

            return $http.post(url, data, globalConfig).then(function (result) {
                return result;
            });
        },

        updateProject: function (url, project) {

            var param = JSON.stringify(project);
            var data = param;

            return $http.put(url, data, globalConfig).then(function (result) {
                return result;
            });
        }

    }

}]);