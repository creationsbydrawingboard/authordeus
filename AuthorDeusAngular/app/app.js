﻿//var appModule = angular.module('appModule', ['ngRoute','HomeController']);

var appModule = angular.module('appModule', ['ngRoute', 'angularNotify', 'ngStorage', 'ngTable', 'cp.ngConfirm', 'summernote', 'angular.filter']);

//API pathway change if necessary
appModule.constant('BASE_USER_ENDPOINT', 'http://localhost:51025/api/');
//appModule.constant('BASE_USER_ENDPOINT', 'https://authordeusapi.azurewebsites.net/api/');