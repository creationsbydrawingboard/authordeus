﻿'USE STRICT';

angular.module('appModule').controller('NotificationController', ['$scope', '$http', 'BASE_USER_ENDPOINT', '$window', '$timeout', '$sessionStorage', '$ngConfirm', 'NotificationService',
    function ($scope, $http, BASE_USER_ENDPOINT, $window, $timeout, $sessionStorage, $ngConfirm, NotificationService) {

        /*<---------------- Declarations --------------> */
        var vm = this;
        var _notificationService = NotificationService;

        vm.viewNotification = viewNotification;

        var notificationurl = BASE_USER_ENDPOINT + 'Notifications';

        vm.currentUser = $sessionStorage.SessionData;
        vm.notificationList;
        vm.listCount = 0;


        /*<---------------- Declarations --------------> */

        /*<---------------- Functions --------------> */


        //Created by: Patrick Sacay
        //Definition: initialize page using notification controller
        function initializePage() {
            if ($sessionStorage.SessionData != undefined || $sessionStorage.SessionData != null) {
                vm.currentUser = $sessionStorage.SessionData;
                getNotifications();
            }
        }

        //Created by: Patrick Sacay
        //Definition: show notification of successful user creation
        function showNotification(ntype, ntext) {
            var notify = {
                type: ntype,
                title: ntext,
                timeout: 1000 //time in ms
            };

            $scope.$emit('notify', notify);
        }

        //Created by: Patrick Sacay
        //Definition: retrieve unread notification
        function getNotifications() {

            _notificationService.getNotificationsByUser(notificationurl, vm.currentUser.UserId).then(function (response) {
                if (response) {

                    var countList = [];

                    for (var i = 0; i < response.length; i++)
                    {
                        if (response[i].IsRead == false)
                        {
                            countList.push(response[i]);
                        }
                    }

                    vm.listCount = countList.length;   
                    vm.notificationList = response;

                }
            })
                .catch(function (err) {
                    showNotification('error', 'Something went wrong with saving notifications. Please contact admin support.');
                });

        }

        //Created by: Patrick Sacay
        //Definition: view notification and tagged them as read
        function viewNotification(notification) {

            notification.IsRead = true;// tag as read by the user

            _notificationService.tagReadNotification(notificationurl, notification).then(function (response) {
                if (response) {
                    vm.notificationList = response;
                }
            })
                .catch(function (err) {
                    showNotification('error', 'Something went wrong with updating notification. Please contact admin support.');
                });


            viewNotificationPage();

        }

         //Created by: Patrick Sacay
        //Definition: transfer page to specific page 
        function viewNotificationPage()
        {
            $window.location.href = '../../../Webforms/Project/ManageProject.aspx';
        }

        /*<---------------- Functions --------------> */

        /*<---------------- Function Calls --------------> */

        initializePage();

        /*<---------------- Function Calls --------------> */

    }]);