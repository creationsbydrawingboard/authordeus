﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AuthorDeusData;

namespace AuthorDeusAPI.Controllers
{
	public class UserTypesController : ApiController
	{
		private AuthorDeusEntities db = new AuthorDeusEntities();

		// GET: api/UserTypes
		public async Task<IHttpActionResult> GetUserType()
		{
			try
			{
				var resp = await (from p in db.UserType
													where p.Name != "Administrator"
													&& p.Void != true
													select p).ToListAsync();

				return Ok(resp);
			}
			catch (System.Exception ex)
			{
        var sample = ex;
				return BadRequest();
			}
			
		}

		// GET: api/UserTypes/5
		[ResponseType(typeof(UserType))]
		public async Task<IHttpActionResult> GetUserType(int id)
		{
			UserType userType = await db.UserType.FindAsync(id);
			if (userType == null)
			{
				return NotFound();
			}

			return Ok(userType);
		}

		// PUT: api/UserTypes/5
		[ResponseType(typeof(void))]
		public async Task<IHttpActionResult> PutUserType(int id, UserType userType)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (id != userType.UserTypeId)
			{
				return BadRequest();
			}

			db.Entry(userType).State = EntityState.Modified;

			try
			{
				await db.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!UserTypeExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return StatusCode(HttpStatusCode.NoContent);
		}

		// POST: api/UserTypes
		[ResponseType(typeof(UserType))]
		public async Task<IHttpActionResult> PostUserType(UserType userType)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			db.UserType.Add(userType);
			await db.SaveChangesAsync();

			return CreatedAtRoute("DefaultApi", new { id = userType.UserTypeId }, userType);
		}

		// DELETE: api/UserTypes/5
		[ResponseType(typeof(UserType))]
		public async Task<IHttpActionResult> DeleteUserType(int id)
		{
			UserType userType = await db.UserType.FindAsync(id);
			if (userType == null)
			{
				return NotFound();
			}

			db.UserType.Remove(userType);
			await db.SaveChangesAsync();

			return Ok(userType);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}

		private bool UserTypeExists(int id)
		{
			return db.UserType.Count(e => e.UserTypeId == id) > 0;
		}
	}
}