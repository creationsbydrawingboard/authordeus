﻿'USE STRICT';

angular.module('appModule').controller('ProjectController', ['$scope', '$http', 'BASE_USER_ENDPOINT', '$window', '$timeout', '$sessionStorage', '$ngConfirm', 'ProjectService', 'ProjectGroupService', 'UserService', 'NotificationService',
    function ($scope, $http, BASE_USER_ENDPOINT, $window, $timeout, $sessionStorage, $ngConfirm, ProjectService, ProjectGroupService, UserService, NotificationService) {

        /*<---------------- Declarations --------------> */

        var vm = this;
        var _projectService = ProjectService;
        var _projectGroupService = ProjectGroupService;
        var _userService = UserService;
        var _notificationService = NotificationService;

        var projecturl = BASE_USER_ENDPOINT + 'Projects';
        var projectgroupurl = BASE_USER_ENDPOINT + 'ProjectGroups';
        var userurl = BASE_USER_ENDPOINT + 'Users';
        var notificationurl = BASE_USER_ENDPOINT + 'Notifications';

        vm.createProject = createProject;
        vm.myFunction = myFunction;
        vm.editProjectDetails = editProjectDetails;
        vm.manageProject = manageProject;
        vm.deleteProject = deleteProject;
        vm.editProjectContent = editProjectContent;
        vm.quoteInvitation = quoteInvitation;

        vm.projectData = {
            OwnerId: "", Name: "", Description: "", Data: "", DateCreated: "", CreatedBy: "", Status: "",
            DateUpdated: "", UpdatedBy: "", Void: false
        };

        vm.projectGroupData =
            {
                ProjectId: "", UserId: "", DateCreated: "", CreatedBy: "",
                DateUpdated: "", UpdatedBy: "", Void: false
            };

        vm.notificationParam =
            {
                Description: "", ReceiverId: "", DateCreated: "", CreatedBy: "",
                DateUpdated: "", UpdatedBy: "", Void: false, IsRead: ""
            };

        vm.projects = [];
        vm.projectsCo = [];
        vm.assigedUsers = [];
        vm.invitedPrinters = [];
        vm.tempInvitedPrinters = [];
        vm.selectedProject = "";

        vm.usersToBeRemoved = [];
        vm.usersToBeAssigned = [];

        vm.currentUser;
        vm.currentProject;

        vm.tempUser;
        vm.pristineTempPrinters = angular.copy(vm.tempInvitedPrinters);

        /*<---------------- Declarations --------------> */

        /*<---------------- Functions --------------> */

        //Created by: Patrick Sacay
        //Definition: initialize page using project controller
        function initializePage() {
            if ($sessionStorage.SessionData != undefined || $sessionStorage.SessionData != null) {
                vm.currentUser = $sessionStorage.SessionData;
            }
        }

        //Created by: Patrick Sacay
        //Definition: show create project modal
        function createProject() {

            var jc = $ngConfirm({
                title: 'New Project',
                icon: 'fa fa-book',
                content: '<div class="form" name="projectform"><div class="form-group" ng-class="{\'has-error\': inputProjectError}">' +
                    '<label>Project Name</label>' +
                    '<input type="text" name="inputprojname" class="form-control" ng-model="projectname" placeholder="Enter project name..">' +
                    '<span class="help-block" ng-show="inputProjectError">Project name is required</span> </div>' +
                    '<div class="form-group">' +
                    '<label>Description</label>' +
                    '<textarea class="form-control" ng-model="projectdescription" rows="3" placeholder="Enter ..."></textarea> </div>' +
                    '<div class="form-group"></div>',
                animation: 'scale',
                columnClass: 'medium',
                buttons: {
                    Create: {
                        btnClass: "btn-blue",
                        action: function (scope) {

                            if (scope.projectname == '' || scope.projectname == undefined) {
                                scope.inputProjectError = true;
                                return false;
                            }
                            else {
                                vm.projectData.Name = scope.projectname;
                                vm.projectData.Description = scope.projectdescription;

                                $ngConfirm({
                                    title: "Confirm",
                                    content: 'Are you sure you want to create this project?',
                                    buttons: {
                                        Yes: {
                                            btnClass: "btn-blue",
                                            action: function () { //// create project record here  
                                                createProjectRecord();
                                                jc.close();
                                            }

                                        },
                                        No: function () { //// cancel creation

                                        }
                                    }
                                });
                                return false;
                            }
                            return false;
                        }
                    },
                    Cancel: {
                        btnClass: "btn-danger",
                    }
                },
                onScopeReady: function (scope) {
                    scope.inputProjectError = false;
                }
            });

        }

        //Created by: Patrick Sacay
        //Definition: show edit project modal
        function editProjectDetails(projectParam) {

            vm.projectData.ProjectId = projectParam.ProjectId;
            vm.projectData.Name = projectParam.Name;
            vm.projectData.Description = projectParam.Description;
            vm.projectData.OwnerId = projectParam.OwnerId;
            vm.projectData.DateCreated = projectParam.DateCreated;
            vm.projectData.CreatedBy = projectParam.CreatedBy;

            var jc = $ngConfirm({
                title: 'Update Project',
                icon: 'fa fa-book',
                content: '<div class="form" name="projectform"><div class="form-group" ng-class="{\'has-error\': inputProjectError}">' +
                    '<label>Project Name</label>' +
                    '<input type="text" name="inputprojname" class="form-control" ng-model="projectname" placeholder="Enter project name..">' +
                    '<span class="help-block" ng-show="inputProjectError">Project name is required</span> </div>' +
                    '<div class="form-group">' +
                    '<label>Description</label>' +
                    '<textarea class="form-control" ng-model="projectdescription" rows="3" placeholder="Enter ..."></textarea> </div>' +
                    '<div class="form-group"></div>',
                animation: 'scale',
                columnClass: 'medium',
                buttons: {
                    Update: {
                        btnClass: "btn-blue",
                        action: function (scope) {
                            if (scope.projectname == '' || scope.projectname == undefined) {
                                scope.inputProjectError = true;
                                return false;
                            }
                            else {
                                vm.projectData.Name = scope.projectname;
                                vm.projectData.Description = scope.projectdescription;
                                updateProjectRecord(false);
                            }
                        }
                    },
                    Cancel: {
                        btnClass: "btn-default"
                    }
                    //,
                    //Delete: {
                    //    text: "Delete this project",
                    //    btnClass: "btn-danger",
                    //    show: checkDeletePermission(),
                    //    action: function () {
                    //        var sjc = $ngConfirm({
                    //            title: "Confirm",
                    //            content: '<p>Deleting this project will also delete co-authors access</p>' +
                    //                '<input type="password" class="form-control" ng-model="deletepassword" ng-change="applychange(deletepassword)" placeholder="Please input your password..">',
                    //            buttons: {
                    //                Yes: {
                    //                    disabled: true,
                    //                    text: "I understand",
                    //                    btnClass: "btn-warning",
                    //                    action: function () { //// delete project record here
                    //                        updateProjectRecord(true);
                    //                        jc.close();
                    //                    }
                    //                },
                    //                No: function () {

                    //                }
                    //            },
                    //            onScopeReady: function (scope) {
                    //                scope.applychange = function () {
                    //                    if (scope.deletepassword == vm.currentUser.Password) {
                    //                        sjc.buttons.Yes.setDisabled(false);
                    //                    }
                    //                }
                    //            }
                    //        });
                    //        return false;
                    //    }
                    //}
                },
                onScopeReady: function (scope) {
                    scope.inputProjectError = false;
                    scope.projectname = projectParam.Name;
                    scope.projectdescription = projectParam.Description;
                }
            });
        }


        //Created by: Patrick Sacay
        //Definition: Delete selected project
        function deleteProject(projectData) {

            vm.projectData = projectData;

            var sjc = $ngConfirm({
                title: "Confirm",
                content: '<p>Deleting this project will also delete co-authors access</p>' +
                    '<input type="password" class="form-control" ng-model="deletepassword" ng-change="applychange(deletepassword)" placeholder="Please input your password..">',
                buttons: {
                    Yes: {
                        disabled: true,
                        text: "I understand",
                        btnClass: "btn-warning",
                        action: function () { //// delete project record here
                            updateProjectRecord(true);
                        }
                    },
                    No: function () {

                    }
                },
                onScopeReady: function (scope) {
                    scope.applychange = function () {
                        if (scope.deletepassword == vm.currentUser.Password) {
                            sjc.buttons.Yes.setDisabled(false);
                        }
                    }
                }
            });
            return false;
        }


        //Created by: Patrick Sacay
        //Definition: edit project content on selected project
        function editProjectContent(projectData) {
            $sessionStorage.SessionDataProject = projectData.ProjectId;
            $window.location.href = '../../../Webforms/Editor/FileEditor.aspx';

        }

        //Created by: Patrick Sacay
        //Definition: load all assigned users from the project
        function manageProject(project) {

            vm.currentProject = project;

            _userService.getAssignedUsers(userurl, vm.currentUser.UserId, project.ProjectId).then(function (response) {
                if (response) {
                    vm.assigedUsers = response;
                    openManageProject();
                }
                else {
                    showNotification('error', 'Something went wrong.');
                }
            })
                .catch(function (err) {
                    showNotification('error', 'Something went wrong.');
                });

        }

        //Created by: Patrick Sacay
        //Definition: open manage project modal
        function openManageProject() {

            var jc = $ngConfirm({
                title: 'Manage Project',
                icon: 'fa fa-book',
                contentUrl: '/Webforms/Project/confirmhtml/manageproject.html',
                columnClass: 'large',
                buttons:
                    {
                        Save:
                            {
                                btnClass: "btn-primary",
                                action: function () {
                                    var sjc = $ngConfirm({
                                        title: "Confirm",
                                        content: 'Are you sure you want to save changes?',
                                        buttons: {
                                            Yes: {
                                                btnClass: "btn-warning",
                                                action: function () {
                                                    removeAssignedUsers().done(function () {
                                                        assignUsers().done(function () {
                                                            showNotification('success', 'Successfully Saved Changes.');
                                                            vm.usersToBeRemoved = [];
                                                            vm.usersToBeAssigned = [];
                                                            jc.close();
                                                        });
                                                    });
                                                }
                                            },
                                            No: function () {
                                            }
                                        }
                                    });
                                    return false;
                                }
                            },
                        Cancel:
                            {
                                action: function () {

                                }
                            }
                    },
                onScopeReady: function (scope) {

                    scope.userlist = vm.assigedUsers;
                    scope.nonexist = false;
                    scope.emailtext = scope.nonexist == false ? "Find" : "Assign";

                    scope.checkEmail = function (inputemail) {

                        for (var i = 0, iLen = scope.userlist.length; i < iLen; i++) {
                            if (scope.userlist[i].Email == inputemail) {
                                $ngConfirm('Email already assigned in this project.');
                                pass = false;
                                return false;
                            }
                        }

                        if (!scope.nonexist) {

                            if (inputemail != vm.currentUser.Email) {
                                validateEmail(inputemail).done(function (response) {
                                    if (response.data != null) {
                                        scope.nonexist = true;
                                        scope.emailtext = scope.nonexist == false ? "Find" : "Assign";
                                        vm.tempUser = response.data;
                                    }
                                    else {
                                        $ngConfirm('User email is not registered in database.');
                                    }
                                });
                            }
                            else {
                                $ngConfirm('Cannot use own email.');
                            }


                        }
                        else {
                            scope.userlist.push(vm.tempUser);

                            scope.nonexist = false;
                            scope.emailtext = scope.nonexist == false ? "Find" : "Assign";
                            scope.inputemail = "";

                            var dateTime = new Date();

                            var assignData =
                                {
                                    ProjectId: vm.currentProject.ProjectId, UserId: vm.tempUser.UserId,
                                    DateCreated: dateTime, CreatedBy: vm.currentUser.Username,
                                    DateUpdated: dateTime, UpdatedBy: vm.currentUser.Username, Void: false
                                };

                            vm.usersToBeAssigned.push(assignData);
                        }


                    };

                    scope.unassignUser = function (index) {

                        var user = scope.userlist[index];
                        var dateTime = new Date();

                        var unassigData =
                            {
                                ProjectId: vm.currentProject.ProjectId, UserId: user.UserId,
                                DateUpdated: dateTime, UpdatedBy: vm.currentUser.Username, Void: true
                            };

                        vm.usersToBeRemoved.push(unassigData);

                        scope.userlist.splice(index, 1);

                    };
                }
            });

        }

        //Created by: Patrick Sacay
        //Definition: quote invitation event
        function quoteInvitation(project) {

            vm.selectedProject = project.ProjectId;

            _userService.getPrinterListForProject(userurl, project.ProjectId).then(function (response) {
                if (response) {
                    vm.invitedPrinters = response.data;
                    openQuoteInvitation();
                }
                else {
                    showNotification('error', 'Something went wrong.');
                }
            })
                .catch(function (err) {
                    showNotification('error', 'Something went wrong.');
                });

        }


        //Created by: Patrick Sacay
        //Definition: open quote invitation modal
        function openQuoteInvitation() {

            vm.tempInvitedPrinters = vm.pristineTempPrinters;

            var jc = $ngConfirm({
                title: 'Qoute Invititation',
                icon: 'fa fa-book',
                closeIcon: true,
                contentUrl: '/Webforms/Project/confirmhtml/qouteinvitation.html',
                columnClass: 'large',
                buttons: {
                    Send:
                        {
                            disabled: true,
                            btnClass: "btn-primary",
                            action: function () {
                                var sjc = $ngConfirm({
                                    title: "Confirm",
                                    content: 'Are you sure you want to send quote invitation to Printers?',
                                    buttons: {
                                        Yes: {
                                            btnClass: "btn-warning",
                                            action: function () {

                                                sendQuoteInvitation().done(function () {

                                                    //create notification here                                                   
                                                    for (var i = 0, iLen = vm.tempInvitedPrinters.length; i < iLen; i++) {                  
                                                        
                                                        var recId = vm.tempInvitedPrinters[i].PrinterId;
                                                        var user =  vm.currentUser.Username;
                                                        var stringParam = user + " invited you to quote their new project.";

                                                        createNotification(stringParam, recId);
                                                    }
                                                  
                                                    showNotification('success', 'Successfully Sent Invitations.');
                                                    jc.close();
                                                });
                                            }
                                        },
                                        No: function () {
                                        }
                                    }
                                });
                                return false;
                            }
                        },
                    Cancel:
                        {
                            btnClass: "btn-default",
                            action: function () {
                                vm.tempInvitedPrinters = vm.pristineTempPrinters;
                            }
                        }
                },
                onScopeReady: function (scope) {

                    scope.userlist = vm.invitedPrinters;
                    scope.quoteInviteData = { ProjectId: "", PrinterId: "", Message: "", DateCreated: "", CreatedBy: "", DateUpdated: "", UpdatedBy: "", IsRead: false, Void: false };
                    scope.pristineQuote = angular.copy(scope.quoteInviteData);

                    scope.sendInvitation = function () {
                        jc.buttons.Send.setDisabled(false);
                    };

                    scope.filterUserList = function (status) {
                        if (status === 'All') {
                            scope.userlist = vm.invitedPrinters;
                        }
                        else if (status === 'Sent') {
                            scope.userlist = vm.invitedPrinters.filter(function (el) {
                                return (el.InviteId != null);
                            });
                        }
                        else {
                            scope.userlist = vm.invitedPrinters.filter(function (el) {
                                return (el.InviteId == null);
                            });
                        }


                    };

                    scope.validateSend = function () {
                        if (vm.tempInvitedPrinters.length != 0 && scope.quoteMessage != "") {
                            jc.buttons.Send.setDisabled(false);

                            for (var i = 0, iLen = vm.tempInvitedPrinters.length; i < iLen; i++) {
                                vm.tempInvitedPrinters[i].Message = scope.quoteMessage;
                            }

                        }
                        else {
                            jc.buttons.Send.setDisabled(true);
                        }
                    }

                    scope.checkboxClick = function (event, value, index) {

                        var checked = event.target.checked;

                        if (checked) {

                            var dateTime = new Date();

                            scope.quoteInviteData.ProjectId = vm.selectedProject;
                            scope.quoteInviteData.PrinterId = value;
                            scope.quoteInviteData.CreatedBy = vm.currentUser.Username;
                            scope.quoteInviteData.UpdatedBy = vm.currentUser.Username;
                            scope.quoteInviteData.DateCreated = dateTime;
                            scope.quoteInviteData.DateUpdated = dateTime;

                            vm.tempInvitedPrinters.push(scope.quoteInviteData);// insert quote data to temp list

                            scope.quoteInviteData = scope.pristineQuote; //clear quote data
                        }
                        else {

                            vm.tempInvitedPrinters.splice(index, 1);

                        }

                        scope.validateSend();

                    };


                }
            });
        }

        //Created by: Patrick Sacay
        //Definition: send quote invitation to printers
        function sendQuoteInvitation() {
            var deferred = new $.Deferred();
            if (vm.tempInvitedPrinters.length) {

                _projectService.createInvitation(projecturl, vm.tempInvitedPrinters).then(function (response) {
                    if (response) {
                        deferred.resolve(true);
                    }
                    else {
                        deferred.resolve(false);
                    }
                })
                    .catch(function (err) {
                        showNotification('error', 'Something went wrong.');
                    });

            }
            else {
                deferred.resolve(true);
            }
            return deferred.promise();
        }

        //Created by: Patrick Sacay
        //Definition: check email if existing in the database
        function validateEmail(emailtext) {

            var deferred = new $.Deferred();

            _userService.verifyEmail(userurl, emailtext).then(function (response) {
                if (response.data != null) {
                    deferred.resolve(response);
                }
                else {
                    deferred.resolve(response);
                }
            })
                .catch(function (err) {
                    showNotification('error', 'Something went wrong.');
                });

            return deferred.promise();

        }

        //Created by: Patrick Sacay
        //Definition: remove assigned user from the project
        function removeAssignedUsers() {
            var deferred = new $.Deferred();

            if (vm.usersToBeRemoved.length) {

                _projectGroupService.unassignUserProject(projectgroupurl, vm.usersToBeRemoved).then(function (response) {
                    if (response) {
                        deferred.resolve(true);
                    }
                    else {
                        deferred.resolve(false);
                    }
                })
                    .catch(function (err) {
                        showNotification('error', 'Something went wrong.');
                    });

            }
            else {
                deferred.resolve(true);
            }

            return deferred.promise();

        }

        //Created by: Patrick Sacay
        //Definition: assign project to the users
        function assignUsers() {
            var deferred = new $.Deferred();
            if (vm.usersToBeAssigned.length) {


                _projectGroupService.assignUserProject(projectgroupurl, vm.usersToBeAssigned).then(function (response) {
                    if (response) {
                        deferred.resolve(true);
                    }
                    else {
                        deferred.resolve(false);
                    }
                })
                    .catch(function (err) {
                        showNotification('error', 'Something went wrong.');
                    });


            }
            else {
                deferred.resolve(true);
            }
            return deferred.promise();
        }

        //Created by: Patrick Sacay
        //Definition: check if user type for permission
        function checkDeletePermission() {
            if (vm.currentUser.UserTypeId == 1000) //// this will check if user is admin
            {
                return true;
            }
            else {
                if (vm.currentUser.UserId == vm.projectData.OwnerId) //// this will check if current user is owner
                {
                    return true;
                }
            }
            return false;
        };

        //Created by: Patrick Sacay
        //Definition: get all projects that user created
        function getAuthoredProject() {
            _projectService.getAuthoredProject(projecturl, vm.currentUser.UserId).then(function (data) {
                //this will execute when the AJAX call completes.
                vm.projects = data;
            });
        }

        //Created by: Patrick Sacay
        //Definition: get all assigned projects to the user
        function getCoAuthoredProject() {
            _projectService.getCoAuthoredProject(projecturl, vm.currentUser.UserId).then(function (data) {
                //this will execute when the AJAX call completes.
                vm.projectsCo = data;
            });
        }

        function myFunction() {
            var x = Math.floor((Math.random() * 100) + 1);
            return x + '%';
        }

        //Created by: Patrick Sacay
        //Definition: create project record in the database
        function createProjectRecord() {
            var dateTime = new Date();

            vm.projectData.OwnerId = vm.currentUser.UserId;
            vm.projectData.DateCreated = dateTime;
            vm.projectData.DateUpdated = dateTime;
            vm.projectData.CreatedBy = vm.currentUser.Username; //change this to session name
            vm.projectData.UpdatedBy = vm.currentUser.Username;
            vm.projectData.Void = false;

            _projectService.createProject(projecturl, vm.projectData).then(function (response) {
                if (response) {
                    vm.projectGroupData.ProjectId = response.data.ProjectId;
                    createProjectGroupRecord();
                }

            })
                .catch(function (err) {
                    showNotification('error', 'Something went wrong');
                });
        }

        //Created by: Patrick Sacay
        //Definition: update project record on database
        function updateProjectRecord(status) {

            var dateTime = new Date();

            vm.projectData.DateUpdated = dateTime;
            vm.projectData.UpdatedBy = vm.currentUser.Username;
            vm.projectData.Void = status;

            _projectService.updateProject(projecturl, vm.projectData).then(function (response) {
                if (response) {
                    if (status) {
                        showNotification('warning', 'Successfully delete project');
                    }
                    else {
                        showNotification('success', 'Successfully Updated Project');
                    }

                    getAuthoredProject();
                    getCoAuthoredProject();

                }

            })
                .catch(function (err) {
                    showNotification('error', 'Something went wrong');
                });
        }

        //Created by: Patrick Sacay
        //Definition: create project group record
        function createProjectGroupRecord() {
            var dateTime = new Date();

            vm.projectGroupData.UserId = vm.currentUser.UserId;
            vm.projectGroupData.DateCreated = dateTime;
            vm.projectGroupData.DateUpdated = dateTime;
            vm.projectGroupData.CreatedBy = vm.currentUser.Username; //change this to session name
            vm.projectGroupData.UpdatedBy = vm.currentUser.Username;
            vm.projectGroupData.Void = false;

            _projectGroupService.createProjectGroup(projectgroupurl, vm.projectGroupData).then(function (response) {
                if (response) {
                    showNotification('success', 'Successfully Created Project');
                    getAuthoredProject();
                    getCoAuthoredProject();
                }
            })
                .catch(function (err) {
                    showNotification('error', 'Something went wrong');
                });
        }

        //Created by: Patrick Sacay
        //Definition: create notification data for specific actions
        function createNotification(desc, recId) {
            var dateTime = new Date();

            vm.notificationParam.Description = desc;
            vm.notificationParam.ReceiverId = recId;
            vm.notificationParam.DateCreated = dateTime;
            vm.notificationParam.DateUpdated = dateTime;
            vm.notificationParam.CreatedBy = vm.currentUser.Username;
            vm.notificationParam.UpdatedBy = vm.currentUser.Username;
            vm.notificationParam.IsRead = false;
            vm.notificationParam.Void = false;

            _notificationService.createNotification(notificationurl, vm.notificationParam).then(function (response) {
                if (response) {
                }
            })
                .catch(function (err) {
                    showNotification('error', 'Something went wrong with saving notifications. Please contact admin support.');
                });
        }


        //Created by: Patrick Sacay
        //Definition: show notification of successful user creation
        function showNotification(ntype, ntext) {
            var notify = {
                type: ntype,
                title: ntext,
                timeout: 1000 //time in ms
            };

            $scope.$emit('notify', notify);
        }

        /*<---------------- Functions --------------> */

        /*<---------------- Function Calls --------------> */
        initializePage();
        getAuthoredProject();
        getCoAuthoredProject();
        /*<---------------- Function Calls --------------> */



    }]);