﻿'USE STRICT'

angular.module('appModule').controller('HomeController', ['$scope', '$sessionStorage', '$window', function ($scope, $sessionStorage, $window) {


    /*<---------------- Declarations --------------> */


    var vm = this;

    vm.createProject = createProject;
    vm.logout = logout;

    vm.currentUser = $sessionStorage.SessionData;

    vm.Firstname = vm.currentUser.FirstName;

    /*<---------------- Declarations --------------> */

    /*<---------------- Functions --------------> */

    function createProject() {

        showNotification('success', 'Successfully Created User');
    }

    function logout()
    {
        $sessionStorage.$reset();
        $window.location.href = '../../Login.aspx';
    };

    //Created by: Patrick Sacay
    //Definition: show notification of successful user creation
    function showNotification(ntype, ntext) {
        var notify = {
            type: ntype,
            title: ntext,
            timeout: 1000 //time in ms
        };

        $scope.$emit('notify', notify);
    }

    /*<---------------- Functions --------------> */

    /*<---------------- Function Calls --------------> */



    /*<---------------- Function Calls -------------->*/


    //alert($sessionStorage.SessionData.UserId);


  

}]);