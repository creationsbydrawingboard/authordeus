﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageProject.aspx.cs" Inherits="AuthorDeusAngular.WebForm3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script src="../../app/services/userService.js"></script>
    <script src="../../app/services/projectService.js"></script>
    <script src="../../app/services/projectGroupService.js"></script>
    <script src="../../app/controllers/project/projectController.js"></script>

    <div ng-controller="ProjectController as projCtrl">
        <div notifybar style="position: absolute; top: 0; right: 0; z-index: 99999999999999999"></div>
        <div class="row">
            <div class="col-lg-2 col-md-2">
                <input type="button" value="Create Project" class="btn btn-primary" ng-click="projCtrl.createProject()"></input>
            </div>
        </div>
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Authored</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="">
                <div ng-repeat="project in projCtrl.projects" ng-if="$index % 4 == 0" class="row">
                    <div ng-repeat="item in projCtrl.projects.slice($parent.$index, ($parent.$index + 4))" class="col-xs-3">
                        <div class="box box-primary">
                            <div class="box-body box-profile">
                                <img class="profile-user-img img-responsive img-circle" src="../../Sources/book.png" runat="server" alt="User profile picture">
                                <a href="#">
                                    <h3 class="profile-username text-center" ng-click="projCtrl.editProjectDetails(item)">{{item.Name}}</h3>
                                </a>
                                <p class="text-muted text-center">{{item.Description || 'N/A'}}</p>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                                        <span class="sr-only">40% Complete (success)</span>
                                    </div>
                                </div>
                                <a href="#" class="btn btn-primary btn-block" ng-click="projCtrl.manageProject(item)"><b>Manage</b></a>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Co-Authored</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="">
                <div ng-repeat="project in projCtrl.projectsCo" ng-if="$index % 4 == 0" class="row">
                    <div ng-repeat="item in projCtrl.projectsCo.slice($parent.$index, ($parent.$index + 4))" class="col-xs-3">
                        <div class="box box-primary">
                            <div class="box-body box-profile">
                                <img class="profile-user-img img-responsive img-circle" src="../../Sources/book.png" runat="server" alt="User profile picture">
                                <a href="#">
                                    <h3 class="profile-username text-center">{{item.Name}}</h3>
                                </a>
                                <p class="text-muted text-center">{{item.Description || 'N/A'}}</p>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                                        <span class="sr-only">40% Complete (success)</span>
                                    </div>
                                </div>
                                <a href="#" class="btn btn-primary btn-block"><b>Continue..</b></a>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</asp:Content>
